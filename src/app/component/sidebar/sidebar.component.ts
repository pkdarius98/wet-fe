import { Router } from '@angular/router';
import { forEach } from 'lodash';
import { StudentService } from './../../services/student.service';
import { Component, OnInit } from '@angular/core';
import { Role } from '../../model/enums';
import { AuthService } from './../../services/auth.service';
import { TeacherService } from '../../services/teacher.service';
import { Class } from '../../model/models';
import { Observable } from 'rxjs';

export interface RouteInfo {
  path?: string;
  title?: string;
  type?: string;
  icontype?: string;
  rtlTitle?: string;
  collapse?: string;
  isCollapsed?: boolean;
  isCollapsing?: any;
  children?: ChildrenItems[];
}
// tslint:disable-next-line: align
export interface ChildrenItems {
  path: string;
  title: string;
  smallTitle?: string;
  type?: string;
  collapse?: string;
  children?: ChildrenItems2[];
  isCollapsed?: boolean;
}
export interface ChildrenItems2 {
  path?: string;
  smallTitle?: string;
  rtlSmallTitle?: string;
  title?: string;
  rtlTitle?: string;
  type?: string;
  collapse?: string;
  children?: ChildrenItems3[];
  isCollapsed?: boolean;
}

export interface ChildrenItems3 {
  path?: string;
  smallTitle?: string;
  rtlSmallTitle?: string;
  title?: string;
  rtlTitle?: string;
  type?: string;
}

const SuperAdmin: RouteInfo[] = [
  {
    path: '/sa/role',
    title: 'Admin Management',
    type: 'link',
    icontype: 'tim-icons icon-badge'
  },
  {
    path: '/sa/analysis',
    title: 'Server Analysis',
    type: 'link',
    icontype: 'tim-icons icon-chart-pie-36',
  }
];

const Admin: RouteInfo[] = [
  {
    path: '/ad/semester',
    title: 'Semesters',
    type: 'link',
    icontype: 'tim-icons icon-calendar-60'
  },
  {
    path: '/ad/classes',
    title: 'Classes',
    type: 'link',
    icontype: 'tim-icons icon-paper'
  },
  {
    path: '/ad/students',
    title: 'Students',
    type: 'link',
    icontype: 'tim-icons icon-satisfied'
  },
  {
    path: '/ad/teachers',
    title: 'Teachers',
    type: 'link',
    icontype: 'tim-icons icon-single-02'
  },
  {
    path: '/ad/subjects',
    title: 'Subjects',
    type: 'link',
    icontype: 'tim-icons icon-book-bookmark'
  },
  {
    path: '/ad/languages',
    title: 'Languages',
    type: 'link',
    icontype: 'tim-icons icon-html5'
  },
  {
    path: '/ad/executable',
    title: 'Executables',
    type: 'link',
    icontype: 'tim-icons icon-spaceship',
  }
];

const Teacher: RouteInfo[] = [
  {
    path: '/tc/class',
    title: 'Classes',
    type: 'sub',
    icontype: 'tim-icons icon-paper',
    collapse: 'class',
    isCollapsed: true,
    children: []
  },
  {
    path: '/tc/workshops',
    title: 'Workshops',
    type: 'link',
    icontype: 'tim-icons icon-notes',
  },
  {
    path: '/tc/submission',
    title: 'Submission',
    type: 'link',
    icontype: 'tim-icons icon-upload',
  }
];

const Student: RouteInfo[] = [
  {
    path: '/st/classes',
    title: 'Classes',
    type: 'sub',
    icontype: 'tim-icons icon-paper',
    collapse: 'class',
    isCollapsed: true,
    children: []
  }
];

export const ROUTES: RouteInfo[] = [].concat.apply([], [SuperAdmin, Admin, Teacher, Student]);

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  superAdminItems: any[];
  adminItems: any[];
  teacherItems: any[];
  studentItems: any[];
  isTeacher: boolean;
  isStudent: boolean;
  isSuperAdmin: boolean;
  isAdmin: boolean;
  roles;
  totalMenu: RouteInfo[];
  classes$: Observable<Class[]>;
  classesSt$: Observable<Class[]>;
  classes = [];
  groups: any;

  constructor(private authService: AuthService, private teacher: TeacherService, private student: StudentService) { }

  ngOnInit(): void {
    this.isSuperAdmin = false;
    this.isAdmin = false;
    this.isStudent = false;
    this.isTeacher = false;
    this.roles = this.authService.getRole().split(',').map(role => role.trim());
    if (this.roles.indexOf(Role.SuperAdmin) !== -1) {
      this.superAdminItems = SuperAdmin.filter(menuItems => menuItems);
    }
    if (this.roles.indexOf(Role.Admin) !== -1) {
      this.adminItems = Admin.filter(menuItems => menuItems);
    }
    if (this.roles.indexOf(Role.Teacher) !== -1) {
      this.teacherItems = Teacher.filter(menuItems => menuItems);
      this.classes$ = this.teacher.getClasses();
      let grouped;
      this.classes$.subscribe((item: any[]) => {
        Teacher.forEach((route: RouteInfo) => {
          if (route.title === 'Classes') {
            route.children = [];
          }
        });
        const mapClass = [];
        item.forEach(ele => {
          const it = {
            classId: ele.classId,
            classCode: ele.classCode,
            semesterName: ele.semester.name,
            subjectCode: ele.subject.subjectCode
          };
          mapClass.push(it);
        });
        grouped = this.groupBy(mapClass, group => group.semesterName);
        grouped.forEach( (value, key) => {
          const child1: ChildrenItems = {
            path: key,
            title: key,
            type: 'sub',
            smallTitle: key.charAt(0),
            collapse: key,
            isCollapsed: true,
            children: []
          };
          const filterCode = this.groupBy(value, group => group.subjectCode);
          filterCode.forEach((valueCode, keyCode) => {
            const child2: ChildrenItems2 = {
              path: keyCode,
              title: keyCode,
              type: 'sub',
              smallTitle: keyCode.charAt(0),
              collapse: keyCode,
              isCollapsed: true,
              children: []
            };
            valueCode.forEach(ite => {
              const child3: ChildrenItems3 = {
              path: ite.classId,
              title: ite.classCode,
              type: 'link',
              smallTitle: ite.classCode
              };
              child2.children.push(child3);
            });
            child1.children.push(child2);
          });
          Teacher.forEach((route: RouteInfo) => {
            if (route.title === 'Classes') {
              route.children.push(child1);
            }
          });
        });
      });
    }
    if (this.roles.indexOf(Role.Student) !== -1) {
      this.studentItems = Student.filter(menuItems => menuItems);
      this.classesSt$ = this.student.getClasses();

      this.classesSt$.subscribe((item: any[]) => {
        Student.forEach((route: RouteInfo) => {
          if (route.title === 'Classes') {
            route.children = [];
          }
        });
        const subjects = [] as any;
        item.forEach(ele => {
          subjects.push({ classCode: ele.classCode, subjectCode: ele.subject.subjectCode, classId:ele.classId });
        });

        let grouped = this.groupBy(subjects, group => group.subjectCode);
        // grouped = this.groupBy(item, group => group.subjectCode);
        grouped.forEach((value, key) => {
          let classId;
          let title;
          value.forEach(i=>{
            classId = i.classId;
            title = i.subjectCode +" - " +i.classCode;
          })
          const child1: ChildrenItems = {
            path: classId,
            title: title,
            type: 'link',
            smallTitle: key.charAt(0),
            collapse: key,
            isCollapsed: true,
            children: []
          }
          console.log(key);
          Student.forEach((route: RouteInfo) => {
            if (route.title === 'Classes') {
              route.children.push(child1);
            }
          });
        });
      });


    }
  }

  groupBy(list: any[], keyGetter) {
    const map = new Map();
    list.forEach((item) => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });
    return map;
  }
}
