import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { CommonLayoutComponent } from '../layouts/common-layout/common-layout.component';
import { NotFoundComponent } from '../component/not-found/not-found/not-found.component';
import { ErrorComponent } from '../component/error/error/error.component';
import { LoginComponent } from '../pages/auth/login/login.component';

import { AuthGuard } from '../helper/auth.guard';
import { LoginAuthGuard } from '../helper/login-auth.guard';
import { Role } from '../model/enums';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', canActivate: [LoginAuthGuard], component: LoginComponent },
  { path: 'error', component: ErrorComponent },
  {
    path: '',
    component: CommonLayoutComponent,
    children: [
      {
        path: 'sa',
        canActivate: [AuthGuard],
        canActivateChild : [AuthGuard],
        data: {
          roles: [
            Role.SuperAdmin
          ]
        },
        loadChildren: () => import('../pages/sa-page/sa-page.module').then(m => m.SaPageModule)
      },
      {
        path: 'ad',
        canActivate: [AuthGuard],
        canActivateChild : [AuthGuard],
        data: {
          roles: [
            Role.Admin
          ]
        },
        loadChildren: () => import('../pages/admin-page/admin-page.module').then(m => m.AdminPageModule)
      },
      {
        path: 'st',
        canActivate: [AuthGuard],
        canActivateChild : [AuthGuard],
        data: {
          roles: [
            Role.Student
          ]
        },
        loadChildren: () => import('../pages/student-page/student-page.module').then(m => m.StudentPageModule)
      },
      {
        path: 'tc',
        canActivate: [AuthGuard],
        data: {
          roles: [
            Role.Teacher
          ]
        },
        loadChildren: () => import('../pages/teacher-page/teacher-page.module').then(m => m.TeacherPageModule)
      }
    ]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
