import { Title } from '@angular/platform-browser';
import { SAServices } from '../../../services/sa.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-storage-analysis',
  templateUrl: './storage-analysis.component.html',
  styleUrls: ['./storage-analysis.component.css']
})
export class StorageAnalysisComponent implements OnInit {

  public canvas: any;
  public ctx;
  ngOnInit(): void {
    this.titleService.setTitle('Server Analysis | Workshop Evaluation');
    this.SAServices.getInformation("AWS/RDS", "FreeStorageSpace").subscribe(response => {
      this.SAServices.draw("chartLineRDSFree", response);
    })
    
    this.SAServices.getInformation("AWS/RDS", "CPUUtilization").subscribe(response => {
      this.SAServices.draw("chartLineRDSCPU", response);
    })
    this.SAServices.getInformation("AWS/RDS", "CPUUtilization").subscribe(response => {
      this.SAServices.draw("chartLineEC2CPU", response);
    })
  }
  constructor(private SAServices: SAServices,
    private titleService: Title) {
   
  }
}
