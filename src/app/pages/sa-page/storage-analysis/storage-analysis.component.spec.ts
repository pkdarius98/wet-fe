import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorageAnalysisComponent } from './storage-analysis.component';

describe('StorageAnalysisComponent', () => {
  let component: StorageAnalysisComponent;
  let fixture: ComponentFixture<StorageAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorageAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorageAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
