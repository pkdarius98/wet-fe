import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from './../../../model/models';
import { Component, OnInit, ViewChild } from '@angular/core';
import swal from 'sweetalert2';
import { SAServices } from '../../../services/sa.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import * as _ from 'lodash';
import { BsModalRef, ModalDirective } from 'ngx-bootstrap/modal';
import { Validators } from '@angular/forms';
import { LocalStorageService } from 'ngx-webstorage';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-role-management',
  templateUrl: './role-management.component.html',
  styleUrls: ['./role-management.component.css']
})
export class RoleManagementComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  @ViewChild('autoShownEdit', { static: false }) autoShownEdit: ModalDirective;
  arrayBuffer: any;
  file: File;
  listUsers = [];
  isImport = false;
  isValidFormSubmitted = false;
  isModalShown = false;
  isEditShown = false;
  currentUserEdit: any;
  focusTouched;
  focusTouched1;
  focusTouched2;
  focusTouched3;
  focusTouched4;
  focusTouched5;
  flagAc: boolean = true;
  flagDe: boolean = true;
  onText: string = 'On';
  offText: string = 'Off';
  colorSwitch: string = 'info';
  data: string;
  searchByStatus: string = "____";
  getStatus = ['All Status', 'Active', 'De-Active'];
  isPageShow: boolean = false;
  private search = new Subject<any>();

  UsersSuper$: Observable<any[]>;
  UsersAdmin$: Observable<User[]>;
  bsModalRef: BsModalRef;

  page = 1;
  totalSize: number;
  User$: Observable<any[]>;
  listUserAll: any;
  listUserOnSearch = [];
  onSearchValue: string = '';
  selectedProvince = 'All Status';


  roleMapping = {
    ROLE_SA: 'Super Admin',
    ROLE_AD: 'Admin',
    ROLE_TC: 'Teacher',
    ROLE_ST: 'Student'
  };

  userForm = this.fb.group({
    fullName: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    username: ['', [Validators.required]]
  });

  editForm = this.fb.group({
    roles: [[], Validators.required],
    status: [[], Validators.required]
  });

  data1 = [
    { id: '0', itemName: 'DeActive' },
    { id: '1', itemName: 'Active' }
  ];

  settings1 = {
    singleSelection: true,
    text: 'Status Select',
    enableSearchFilter: false,
    classes: 'selectpicker btn-primary'
  };
  selectedItems1 = [];

  data2 = [

    { id: 'ROLE_AD', itemName: 'Admin' }
  ];
  selectedItems2 = [];
  settings2 = {
    singleSelection: false,
    text: 'Choose Roles',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: 'selectpicker btn-info'
  };
  maxSize = 5;

  constructor(private saServices: SAServices,
    private fb: FormBuilder,
    private localStorage: LocalStorageService,
    private spinner: NgxSpinnerService,
    private router:Router,
    private titleService: Title) {

  }
  searchTerm(term: string) {
    this.search.next(term);
    
  }



  ngOnInit(): void {
    this.titleService.setTitle('Admin Management | Workshop Evaluation');
    this.getAllUser();
  }

  showModal(): void {
    this.isModalShown = true;
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

  onHidden(): void {
    this.isModalShown = false;
    this.isValidFormSubmitted = false;
    this.resetForm(this.userForm);
  }

  focusT() {
    this.focusTouched = true;
  }

  showEdit(): void {
    this.isEditShown = true;
  }

  onEditHidden(): void {
    this.isEditShown = false;
    this.resetForm(this.editForm);
  }

  hideEdit(): void {
    this.autoShownEdit.hide();
  }

  onSubmit() {
    this.isValidFormSubmitted = true;
    if (!this.userForm.valid) {
      return;
    }

    const user: User = {
      fullName: this.userForm.value.fullName,
      username: this.userForm.value.username,
      email: this.userForm.value.email
    };
    this.saServices.addUser(user)
      .subscribe(
        data => console.log(data)
        , error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        },
        () => {
          swal.fire({
            title: 'Success!',
            text: 'Create user success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.hideModal();
            this.getAllUser();
          });
          
        }
      );
  }

  onSubmitEdit(): void {
    this.isValidFormSubmitted = true;
    if (!this.editForm.valid) {
      return;
    }
    const user: User = {
      fullName: this.currentUserEdit.fullName,
      username: this.currentUserEdit.username,
      email: this.currentUserEdit.email,
      roles: '',
      enabled: this.editForm.value.status[0].id == 1
    };
    const temp = [];
    this.editForm.value.roles.forEach(item => {
      temp.push(item.id);
    });
    user.roles = temp.join(', ');
    this.saServices.updateUser(user)
      .subscribe(
        data => console.log(data)
        , error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        },
        () => {
          swal.fire({
            title: 'Success!',
            text: 'Update user success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.hideEdit();
          this.getAllUser();
          });
          
        }
      );
  }
  onKey(event) {
    this.data = event.target.value;
    this.userForm.get('email').setValue(this.data + '@fpt.edu.vn');
  }
  resetForm(form: FormGroup) {
    form.reset();
  }

  get editF() {
    return this.editForm.controls;
  }

  get userF() {
    return this.userForm.controls;
  }

  get email() {
    return this.userForm.get('email');
  }

  get roles() {
    return this.userForm.get('roles');
  }

  get fullName() {
    return this.userForm.get('fullName');
  }
  getStatusData(event) {
    if (event == "Active") {
      this.searchByStatus = '';
    } else if (event == "All Status") {
      this.searchByStatus = '____';
    }
    else {
      this.searchByStatus = '_0__';
    }
    this.spinner.show();
    this.UsersSuper$ = this.saServices.onSearchUser(this.page, "", this.searchByStatus);
    this.UsersSuper$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.enabled = item.enabled ? 1 : 0;
      });
      this.listUserOnSearch = data[2];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
  }
  searchUser() {
    this.UsersSuper$ = this.search.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {this.spinner.show()}),
      switchMap((term: string) => this.saServices.onSearchUser(this.page, term, this.searchByStatus)));
    this.listUsers = [];
    this.UsersSuper$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.enabled = item.enabled ? 1 : 0;
      });
      this.listUserOnSearch = data[2];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
  }
  getAllUser() {
    this.spinner.show();
    this.listUsers = [];
    this.UsersSuper$ = this.saServices.onSearchUser(this.page, "", this.searchByStatus);
    this.getListUser();
  }
  getListUser() {
    this.spinner.show();
    this.UsersSuper$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.enabled = item.enabled ? 1 : 0;
      });
      this.listUsers = data[2];
      this.listUserOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
    });
    this.searchUser();
  }


  handler(type: string, $event: ModalDirective) {
    this.resetForm(this.userForm);
  }

  getRoles(user: any) {
    let roles = [];
    for (const role in this.roleMapping) {
      if (user.roles.includes(role)) {
        roles.push(this.roleMapping[role]);
      }
    }
    return roles.join(', ');
  }

  submitAdmin() {
    this.saServices.importAdmin(this.listUsers);
    this.isImport = false;
    this.getListUser();
  }

  upgradeSa(index) {
    swal.fire({
      title: 'Are you sure to upgrade super admin?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.saServices.upgradeSa(this.listUserOnSearch[index].username).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Upgrade super admin success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          });
          this.localStorage.clear('roles');
          this.localStorage.clear('refreshToken');
          this.localStorage.clear('authenticationToken');
          this.router.navigate([`/login`]);
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        });
      }
    });
  }
  Active(index) {
    swal.fire({
      title: 'Are you sure to activate Admin?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.saServices.activeUser(this.listUserOnSearch[index].username).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Activate admin success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.getAllUser();
          });
          
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        })
      }
    })
  }
  deActive(index) {
    swal.fire({
      title: 'Are you sure to deactivate Admin?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.saServices.deactiveUser(this.listUserOnSearch[index].username).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Deactivate admin success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.getAllUser();
          });
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
          this.spinner.hide();
        })
      }
    })
  }
  pageChanged(event: any): void {
    this.getAllUser();
  }
}
