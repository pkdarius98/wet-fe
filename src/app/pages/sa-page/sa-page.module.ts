import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SuperAdminRoutes } from './sa.routing';

import { RoleManagementComponent } from './role-management/role-management.component';
import { StorageAnalysisComponent } from './storage-analysis/storage-analysis.component';

import { ComponentModule } from '../../component/component.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { PaginationModule } from "ngx-bootstrap/pagination";
import { TooltipModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzEmptyModule } from 'ng-zorro-antd/empty';

@NgModule({
  declarations: [
    RoleManagementComponent,
    StorageAnalysisComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(SuperAdminRoutes),
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    ComponentModule,
    AngularMultiSelectModule,
    JwBootstrapSwitchNg2Module,
    PaginationModule.forRoot(),
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    NzSelectModule,
    NgxSpinnerModule,
    NzIconModule,
    NzEmptyModule
  ]
})
export class SaPageModule { }
