import { Routes } from '@angular/router';

import { RoleManagementComponent } from './role-management/role-management.component';
import { StorageAnalysisComponent } from './storage-analysis/storage-analysis.component';

export const SuperAdminRoutes: Routes = [
  {
    path: 'role',
    component: RoleManagementComponent
  },
  {
    path: 'analysis',
    component: StorageAnalysisComponent
  }
];
