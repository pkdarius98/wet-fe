export interface GoogleLoginPayload {
  token: string;
}
