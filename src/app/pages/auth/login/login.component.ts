import { Title } from '@angular/platform-browser';
import {AfterViewInit, Component, OnDestroy, OnInit, NgZone} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginRequestPayload} from './login-request.payload';
import {AuthService} from '../../../services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {throwError} from 'rxjs';
import GoogleAuth = gapi.auth2.GoogleAuth;
import {GoogleLoginPayload} from './google-login-payload';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalStorageService } from 'ngx-webstorage';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {
  isError: boolean;
  loading = false;
  returnUrl: string;
  roleByRank = {
    ROLE_SA: 4,
    ROLE_AD: 3,
    ROLE_TC: 2,
    ROLE_SR: 1
  };

  ngAfterViewInit() {
    gapi.signin2.render('my-signin2', {
      scope: 'profile email',
      longtitle: true,
      theme: 'dark',
      onsuccess: param => this.onSignIn(param)
    });
  }

  onSignIn(googleUser) {
    const profile = googleUser.getBasicProfile();
    this.localStorage.store('avatar', profile.getImageUrl());
    const idToken = googleUser.getAuthResponse().id_token;
    const googleLoginPayload: GoogleLoginPayload = {
      token: idToken
    };
    this.spinner.show();
    this.authService.loginWithGoogle(googleLoginPayload).subscribe(data => {},
      error => {
      this.isError = true;
      this.spinner.hide();
      if (error.status === 403) {
        Swal.fire({
          title: error.status,
          text: 'Your account does not exist or disabled',
          type: 'error',
        });
      }
    },
    () => this.ngZone.run(() => {
      this.isError = false;
      this.spinner.hide();
      const rolesText = this.authService.getRole();
      const roles = rolesText.split(',');
      const listRank = [];
      roles.forEach(role => {
        listRank.push(this.roleByRank[role.trim()]);
      });
      const maxRank = Math.max(...listRank);
      if (maxRank === 4) {
        this.router.navigate(['sa', 'role']);
      } else if (maxRank === 3) {
        this.router.navigate(['ad', 'semester']);
      } else if (maxRank === 2) {
        this.router.navigate(['tc', 'workshops']);
      } else {
        this.router.navigate(['st', 'classes']);
      }
    }));
  }

  constructor(private authService: AuthService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private toastr: ToastrService,
              private ngZone: NgZone,
              private spinner: NgxSpinnerService,
              private localStorage: LocalStorageService,
              private titleService: Title) {
  }

  ngOnInit(): void {
    this.titleService.setTitle('Login | Workshop Evaluation');
    // get return url from route parameters or default to '/'
    // tslint:disable-next-line: no-string-literal
    this.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'] || '/';
  }
}
