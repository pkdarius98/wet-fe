import { Observable } from 'rxjs';
import { TeacherService } from './../../../services/teacher.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Chart } from 'chart.js';
import { JudgingRun } from '../../../model/models';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-submission-detail',
  templateUrl: './submission-detail.component.html',
  styleUrls: ['./submission-detail.component.css']
})
export class SubmissionDetailComponent implements OnInit, AfterViewInit {
  id: any;
  SubmissionDetail$: Observable<any[]>;
  listSubmissionDetail: any = [];
  isShow = false;
  runTimeTotal: number;
  startTime: any;
  endTime: any;
  isModalShown = false;
  chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
  };
  detailRun: JudgingRun;
  metaData: any;
  outDifferent: any;
  outError: any;
  outRun: any;
  outSystem: any;
  constructor(
    private Activatedroute: ActivatedRoute,
    private teacher: TeacherService,
    private titleService: Title
  ) { }

  ngOnInit(): void {
    this.titleService.setTitle('Teacher Submission Detail | Workshop Evaluation');
    this.Activatedroute.paramMap.subscribe(item => {
      this.id = item.get('id');
    });
    this.getSubmission();
  }

  showModal(): void {
    this.isModalShown = true;
  }

  onHidden(): void {
    this.isModalShown = false;
  }

  getSubmission() {
    this.listSubmissionDetail = [];
    this.SubmissionDetail$ = this.teacher.getSubmissionDetail(this.id);
    this.SubmissionDetail$.subscribe((data: any) => {
      this.listSubmissionDetail = data;
      this.isShow = true;
      data.submitTime = new Date(data.submitTime * 1000).toDateString();
      this.runTimeTotal = Math.round((data.judging.endTime - data.judging.startTime) * 100) / 100;
      this.startTime = new Date(data.judging.startTime * 1000).toLocaleTimeString();
      this.endTime = new Date(data.judging.endTime * 1000).toLocaleTimeString();

      setTimeout(() => {
        this.drawCanvas(this.listSubmissionDetail.judging.judgingRun);
      }, 0);
    });
  }
  drawCanvas(data: JudgingRun[]) {
    const colors = [];
    const times = [];
    const labels = [];
    data.forEach((judgingRun: any, index: number) => {
      const runTime = judgingRun.runTime || 0;
      times.push(runTime);
      const result = judgingRun.runResult === 'correct';
      colors.push(result ? 'green' : 'red');
      labels.push(index);
    });

    const color = Chart.helpers.color;
    const barChartData = {
      labels,
      datasets: [{
        label: 'Run time',
        backgroundColor: colors,
        borderColor: colors,
        borderWidth: 1,
        data: times
      }]
    };
    const canvas = document.getElementById('canvas') as HTMLCanvasElement;
    const ctx = canvas.getContext('2d');
    const myChart = new Chart(ctx, {
      type: 'bar',
      data: barChartData,
      options: {
        responsive: true,
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Chart.js Bar Chart'
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
    canvas.onclick =  (evt) => {
      const activePoints = myChart.getElementsAtEvent(evt);
      if (activePoints[0]) {
        const chartData = activePoints[0]._chart.config.data;
        const idx = activePoints[0]._index;
        const label = chartData.labels[idx];
        const value = chartData.datasets[0].data[idx];
        this.detailRun = data[label];
        this.metaData = atob(this.detailRun.judgingRunOutput.metadata);
        this.outDifferent = atob(this.detailRun.judgingRunOutput.outputDifferent);
        this.outError = atob(this.detailRun.judgingRunOutput.outputError);
        this.outRun = atob(this.detailRun.judgingRunOutput.outputRun);
        this.outSystem = atob(this.detailRun.judgingRunOutput.outputSystem);
        this.showModal();
      }
    };
  }
  ngAfterViewInit() {

  }
}
