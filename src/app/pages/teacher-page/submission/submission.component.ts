import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { forEach } from 'lodash';
import { TeacherService } from './../../../services/teacher.service';
import { LocalStorageService } from 'ngx-webstorage';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild } from '@angular/core';
import swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-submission',
  templateUrl: './submission.component.html',
  styleUrls: ['./submission.component.css']
})
export class SubmissionComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  isModalShown = false;
  isValidFormSubmitted = false;
  detailFile: any;
  focusTouched1 = false;
  getListWS = [];
  fileBase64: any;
  selectedProvince = null;
  getWorkshopId: any;
  getFileName = [];
  listSubmission = [];
  totalSize: number;
  submission$: Observable<any[]>;
  page = 1;
  listSubmissionDetail: any;
  isPageShow: boolean = false;
  files = [];
  fileDTOs = [];
  constructor(
    private fb: FormBuilder,
    private localStorage: LocalStorageService,
    private teacher: TeacherService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Teacher Submissions | Workshop Evaluation');
    this.teacher.getListWorkShop(0, 999).subscribe(data => {
      data.data.forEach(i => {
        this.getListWS.push({
          id: i.workshopId,
          name: i.name
        });
      });
    });
    this.getAllSubmission();
  }

  submitForm = this.fb.group({
    files: ['', Validators.required],
    workshop: ['', Validators.required]
  });
  resetForm(form: FormGroup) {
    form.reset();
  }
  get submitF() {
    return this.submitForm.controls;
  }


  provinceChange(value: any): void {
    if (value != null) {
      this.getWorkshopId = value.id;
    }

  }
  showModal(): void {
    this.isModalShown = true;
  }

 

  onHidden(): void {
    this.isModalShown = false;
    this.resetForm(this.submitForm);
    this.getFileName = [];
    this.fileDTOs = [];
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

  onSubmit() {
    // this.isValidFormSubmitted = true;
    if (!this.submitForm.valid) {
      return;
    }

    const submitType = {
      fileDTOs: this.fileDTOs,
      workshopId: this.getWorkshopId
    }
    this.teacher.submission(submitType).subscribe(data => {
      swal.fire({
        title: 'Success!',
        text: 'Submit success',
        type: 'success',
        confirmButtonClass: 'btn btn-success',
      });
      this.hideModal();
      this.getAllSubmission();
    }, error => {
      swal.fire({
        title: 'Error',
        text: 'Error to Submit',
        type: 'error',
        confirmButtonClass: 'btn btn-info',
      });
    })
  }
  onFileChange(event) {
    this.files = event.target.files;
    Array.prototype.forEach.call(this.files, file => {
      const reader = new FileReader();
      this.getFileName.push(file.name);
      const fileDTO = {
        fileName: file.name,
        sourceCode: null,
      }
      reader.readAsDataURL(file);
      if (file.name.substring(file.name.lastIndexOf('.')) !== '.c' && file.name.substring(file.name.lastIndexOf('.')) !== '.java') {
        swal.fire({
          title: 'Invalid file extension',
          text: 'Only accept .c and .java',
          type: 'error',
        });
      } else {
        reader.onload = () => {
          this.fileBase64 = reader.result;
          this.fileBase64 = this.fileBase64.split(',')[1];
          fileDTO.sourceCode = this.fileBase64;
        };
        this.fileDTOs.push(fileDTO);

      }
    });
    const x = document.getElementById(`upload-ws`);
    x.innerHTML = this.getFileName.toString();
    if (this.getFileName.length > 1) {
      x.innerHTML = x.innerHTML.split(',')[1] + ',...';
    } else {
      x.innerHTML = this.getFileName.toString();
    }
  }
  getAllSubmission() {
    this.spinner.show();
    this.submission$ = this.teacher.getSubmission(this.page);
    this.submission$.subscribe((data: any[]) => {
      data[2].forEach(item => {
        item.submitTime = new Date(item.submitTime * 1000).toDateString();
      })
      this.listSubmission = data[2];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
      this.spinner.hide();
    });
    //this.searchStudent();
  }
  pageChanged(event: any): void {
    this.getAllSubmission();
  }
  showModalEdit(index) {
    this.listSubmissionDetail = this.listSubmission[index];
    this.router.navigate([`tc/submission-detail/${this.listSubmissionDetail.submissionId}`]);
  }

  refresh() {
    this.getAllSubmission();
  }
}
