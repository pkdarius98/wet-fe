import { Component, OnInit } from '@angular/core';
import { TeacherService } from '../../../services/teacher.service';
import { AdminService } from '../../../services/admin.service';
import { Executable, Workshop, Category, Subject } from '../../../model/models';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { TagInputModule } from 'ngx-chips';
import { AuthModule } from '../../auth/auth.module';
import { HttpClient } from '@angular/common/http';
import { map, tap, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-workshop-add',
  templateUrl: './workshop-add.component.html',
  styleUrls: ['./workshop-add.component.scss']
})
export class WorkshopAddComponent implements OnInit {

  executables: Executable[];
  workshopID: any;
  scripts: Executable[];
  compares: Executable[];

  focusTouched;
  focusTouched1;
  focusTouched2;
  focusTouched3;
  focusTouched4;
  focusTouched5;
  focusTouched6;
  focusTouched7;
  focusTouched8;
  isValidFormSubmitted = false;

  workshopForm: FormGroup;
  fileBase64: any;

  tagCategories$: Observable<Category[]>;
  subjects$: Observable<Subject[]>;
  isShow = false;
  listOfSelectedValue: string[] = [];

  constructor(
    private http: HttpClient,
    private teacher: TeacherService,
    private fb: FormBuilder,
    private router: Router,
    private admin: AdminService,
    private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Add workshop | Workshop Evaluation');
    this.workshopForm = this.fb.group({
      name: ['', Validators.required],
      timeLimit: ['', Validators.required],
      memoriesLimit: [2097152],
      subjectCode: ['', Validators.required],
      categories: [[], Validators.required],
      outputLimit: [8192],
      content: ['', Validators.required],
      specialRun: ['', Validators.required],
      specialCompare: ['', Validators.required],
      specialCompareArgs: [''],
      combinedRunCompare: [false]
    });
    this.subjects$ = this.teacher.getAllSubject();
    this.admin.getExecutable().subscribe(executables => {
      this.executables = executables;
      this.scripts =  this.executables.filter(item => {
        return item.type === 'run';
      });
      this.compares = this.executables.filter(item => {
        return item.type === 'compare';
      });
    });
  }

  changeSubject(event) {
    if (event) {
      this.isShow = true;
      this.tagCategories$ = this.teacher.getCategory(event);
    }
  }

  handleCharacterE(e) {
    return e.keyCode !== 69;
  }

  get workshopF() {
    return this.workshopForm.controls;
  }

  get name() {
    return this.workshopForm.get('name');
  }

  get timeLimit() {
    return this.workshopForm.get('timeLimit');
  }

  get memoriesLimit() {
    return this.workshopForm.get('memoriesLimit');
  }

  get subjectCode() {
    return this.workshopForm.get('subjectCode');
  }

  get categories() {
    return this.workshopForm.get('categories');
  }

  get outputLimit() {
    return this.workshopForm.get('outputLimit');
  }

  get content() {
    return this.workshopForm.get('content');
  }

  get specialRun() {
    return this.workshopForm.get('specialRun');
  }

  get specialCompare() {
    return this.workshopForm.get('specialCompare');
  }

  get specialCompareArgs() {
    return this.workshopForm.get('specialCompareArgs');
  }

  get combinedRunCompare() {
    return this.workshopForm.get('combinedRunCompare');
  }

  handleFileInput(event: { target: { files: any[]; }; }) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    if (file.name.substring(file.name.lastIndexOf('.')) !== '.pdf') {
      Swal.fire({
        title: 'Invalid file extension',
        text: 'Only accept pdf type',
        type: 'error',
      });
    } else {
      reader.onload = () => {
        this.fileBase64 = reader.result;
      };
      const x = document.getElementById(`upload_workshop`);
      x.innerHTML = file.name;
    }
  }

  onSubmit() {
    this.isValidFormSubmitted = true;
    if (!this.workshopForm.valid) {
      return;
    }
    const obj = this.workshopForm.value;
    obj.content = this.fileBase64.split(',')[1];
    this.teacher.addWorkshop(obj)
    .subscribe(
      data => {
        this.workshopID = data.workshopId;
      },
      error => {
        Swal.fire({
          title: error.error.apierror.status,
          text: error.error.apierror.message,
          type: 'error'
        });
      },
      () => {
        Swal.fire({
          title: 'Success!',
          text: 'Add workshop success',
          type: 'success',
          confirmButtonClass: 'btn btn-success'
        });
        this.router.navigate(['/tc', 'workshops', this.workshopID]);
      }
    );
  }
}
