import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { Workshop, Executable, Category, Subject } from '../../../model/models';
import { TeacherService } from 'src/app/services/teacher.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AdminService } from 'src/app/services/admin.service';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { NzMessageService } from 'ng-zorro-antd/message';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-workshop-edit',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './workshop-edit.component.html',
  styleUrls: ['./workshop-edit.component.css']
})
export class WorkshopEditComponent implements OnInit {

  private sub: any;
  workshopID: any;
  workshop$: Observable<Workshop>;
  workshop = {} as Workshop;

  executables: Executable[];
  scripts = [];
  selectedScript = [];

  compares = [];
  selectedCompare = [];

  fileList: NzUploadFile[] = [];
  fileBase64: any;
  tagCategories$: Observable<Category[]>;
  subjects$: Observable<Subject[]>;
  workshopForm: FormGroup;

  focusTouched;
  focusTouched1;
  focusTouched2;
  focusTouched3;
  focusTouched4;
  focusTouched5;
  focusTouched6;
  focusTouched7;
  focusTouched8;
  isValidFormSubmitted = false;
  file: NzUploadFile = {
    uid: '',
    name: ''
    };
  currentFile: any;
  isShow = false;

  constructor(
    private teacher: TeacherService,
    private route: ActivatedRoute,
    private admin: AdminService,
    private msg: NzMessageService,
    private fb: FormBuilder,
    private titleService: Title,
    private sanitizer: DomSanitizer
    ) { }

  ngOnInit(): void {
    this.titleService.setTitle('Edit Workshop | Workshop Evaluation');
    this.subjects$ = this.teacher.getAllSubject();
    this.admin.getExecutable().subscribe(executables => {
      this.executables = executables;
      this.scripts =  this.executables.filter(item => {
        return item.type === 'run';
      });
      this.compares = this.executables.filter(item => {
        return item.type === 'compare';
      });
    });
    this.sub = this.route.params.subscribe(params => {
      this.workshopID = params.id;
    });
    this.workshop$ = this.teacher.getWorkshopById(this.workshopID);
    this.workshop$.subscribe(item => {
      this.tagCategories$ = this.teacher.getCategory(item.subjectCode);
      this.workshopForm = this.fb.group({
        name: [item.name, Validators.required],
        timeLimit: [item.timeLimit, Validators.required],
        memoriesLimit: [item.memoriesLimit],
        categories: [item.categories, Validators.required],
        outputLimit: [item.outputLimit],
        content: [''],
        specialRun: [item.specialRun, Validators.required],
        specialCompare: [item.specialCompare, Validators.required],
        specialCompareArgs: [item.specialCompareArgs],
        combinedRunCompare: [item.combinedRunCompare]
      });
      console.log(item.categories);
      this.workshop = item;
      this.fileBase64 = `data:application/pdf;base64,${item.content}`;
    });
  }

  handleFileInput(event: { target: { files: any[]; }; }) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    if (file.name.substring(file.name.lastIndexOf('.')) !== '.pdf') {
      Swal.fire({
        title: 'Invalid file extension',
        text: 'Only accept pdf type',
        type: 'error',
      });
    } else {
      reader.onload = () => {
        this.fileBase64 = reader.result;
      };
      const x = document.getElementById(`upload_workshop`);
      x.innerHTML = file.name;
    }
  }

  cancel() {
    this.isShow = false;
    this.fileBase64 = this.currentFile;
  }

  upload() {
    this.isShow = true;
    this.currentFile = this.fileBase64;
    this.fileBase64 = '';
  }

  get workshopF() {
    return this.workshopForm.controls;
  }

  get name() {
    return this.workshopForm.get('name');
  }

  get timeLimit() {
    return this.workshopForm.get('timeLimit');
  }

  get memoriesLimit() {
    return this.workshopForm.get('memoriesLimit');
  }

  get subjectCode() {
    return this.workshopForm.get('subjectCode');
  }

  get categories() {
    return this.workshopForm.get('categories');
  }

  get outputLimit() {
    return this.workshopForm.get('outputLimit');
  }

  get specialRun() {
    return this.workshopForm.get('specialRun');
  }

  get specialCompare() {
    return this.workshopForm.get('specialCompare');
  }

  get specialCompareArgs() {
    return this.workshopForm.get('specialCompareArgs');
  }

  get combinedRunCompare() {
    return this.workshopForm.get('combinedRunCompare');
  }

  changeSubject(event) {
    if (event) {
      this.tagCategories$ = this.teacher.getCategory(event);
    }
  }

  beforeUpload = (event) => {
    console.log(event);
  //   this.fileList = [];
  //   this.fileList = this.fileList.concat(file);
  //   const reader = new FileReader();
  //   reader.readAsDataURL(file);
  //   if (file.name.substring(file.name.lastIndexOf('.')) !== '.pdf') {
  //     this.fileList[0].status = 'error';
  //     this.fileList[0].response = 'Only accept .pdf type';
  //     this.msg.error('Only accept .pdf type');
  //   } else {
  //     reader.onload = () => {
  //       this.fileBase64 = reader.result;
  //   };
  // }
  //   return false;
  }

  handleCharacterE(e) {
    return e.keyCode !== 69;
  }

  download(file: NzUploadFile) {
    const downloadLink = document.createElement('a');
    const fileName = file.name;
    downloadLink.href = file.url;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  onSubmit() {
    this.isValidFormSubmitted = true;
    if (!this.workshopForm.valid) {
      return;
    }
    this.workshop = this.workshopForm.value;
    this.workshop.workshopId = this.workshopID;
    this.workshop.subjectCode = 'PRO192';
    this.workshop.content = this.fileBase64.split(',')[1];
    this.teacher.editWorkshop(this.workshop)
    .subscribe(
      data => {
        this.workshopForm = this.fb.group({
          name: [data.name, Validators.required],
          timeLimit: [data.timeLimit, Validators.required],
          memoriesLimit: [data.memoriesLimit],
          categories: [data.categories, Validators.required],
          outputLimit: [data.outputLimit],
          content: [''],
          specialRun: [data.specialRun, Validators.required],
          specialCompare: [data.specialCompare, Validators.required],
          specialCompareArgs: [data.specialCompareArgs],
          combinedRunCompare: [data.combinedRunCompare]
        });
        this.workshop = data;
        this.fileBase64 = `data:application/pdf;base64,${data.content}`;
        this.isShow = false;
      },
      error => {
        Swal.fire({
          title: error.error.apierror.status,
          text: error.error.apierror.message,
          type: 'error'
        });
      },
      () => {
        Swal.fire({
          title: 'Success!',
          text: 'Update workshop success',
          type: 'success',
          confirmButtonClass: 'btn btn-success'
        });
      }
    );
  }

}
