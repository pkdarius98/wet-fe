import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionType } from '../../../model/enums';
import { WorkshopOv, WorkshopZipfile, Subject, Category, WorkshopUpload } from '../../../model/models';
import { TeacherService } from '../../../services/teacher.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Router } from '@angular/router';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ModalDirective } from 'ngx-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-workshops',
  templateUrl: './workshops.component.html',
  styleUrls: ['./workshops.component.css']
})
export class WorkshopsComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  entries = 10;
  loadingIndicator = true;
  selected: any[] = [];
  temp = [];
  activeRow: any;
  SelectionType = SelectionType;
  rows: WorkshopOv[] = [];
  ColumnMode = ColumnMode;
  listFile64: WorkshopZipfile[] = [];
  reorderable = true;
  workshops: any[] = [];

  isShowPagging = false;
  totalItems: number;
  currentPage = 1;
  perPage = 10;
  searchTerm = '';
  tagCategories$: Observable<Category[]>;
  subjects$: Observable<Subject[]>;
  isShow = false;
  selectedCategory;
  subjectCode;
  workshopID;
  uploading = false;
  fileList: NzUploadFile[] = [];
  maxSize = 5;
  isModalShown = false;
  workshopResult: WorkshopUpload;

  constructor(
    private teacher: TeacherService,
    private router: Router,
    private msg: NzMessageService,
    private spinner: NgxSpinnerService,
    private titleService: Title
    ) { }

  ngOnInit(): void {
    this.titleService.setTitle('Workshops | Workshop Evaluation');
    this.subjects$ = this.teacher.getAllSubject();
    this.spinner.show();
    this.teacher.getListWorkShop(this.currentPage - 1, this.perPage).subscribe(data => {
      this.spinner.hide();
      this.workshops = data.data;
      this.totalItems = data.totalElements;
      if (this.totalItems > this.perPage) {
        this.isShowPagging = true;
      }
    });
  }

  beforeUpload = (file): boolean => {
    this.fileList = this.fileList.concat(file);
    const reader = new FileReader();
    reader.readAsDataURL(file);
    if (file.name.substring(file.name.lastIndexOf('.')) !== '.zip') {
      this.fileList[0].status = 'error';
      this.fileList[0].response = 'Only accept .zip type';
      this.msg.error('Only accept .zip type');
    } else {
      reader.onload = () => {
        const bs64: any = reader.result;
        const fileBase64: WorkshopZipfile = {};
        fileBase64.zipFileBase64 = bs64.split(',')[1];
        fileBase64.fileName = file.name;
        this.listFile64.push(fileBase64);
    };
  }
    return false;
  }

  showModal(): void {
    this.isModalShown = true;
  }

  onHidden(): void {
    this.isModalShown = false;
  }

  handleUpload(): void {
    this.uploading = true;
    this.teacher.uploadZipFile(this.listFile64)
    .subscribe(
      data => {
        this.workshopResult = data;
        if (this.workshopResult.savedWorkshops.length !== 0) {
          this.workshops.push(this.workshopResult.savedWorkshops);
        }
        this.isModalShown = true;
        this.uploading = false;
        this.fileList = [];
        this.listFile64 = [];
        this.spinner.show();
        this.teacher.getListWorkShop(0, this.perPage).subscribe(data1 => {
          this.spinner.hide();
          this.workshops = data1.data;
          this.totalItems = data1.totalElements;
          if (this.totalItems > this.perPage) {
            this.isShowPagging = true;
          }
        });
      }
    );
  }

  changeSubject(event) {
    if (!event) {
      this.isShow = false;
      this.selectedCategory = '';
    }
    this.selectedCategory = '';
    this.isShow = true;
    this.tagCategories$ = this.teacher.getCategory(event);
    this.teacher.getListWorkShop(0, this.perPage, event).subscribe(data => {
      this.workshops = data.data;
      this.totalItems = data.totalElements;
      if (this.totalItems > this.perPage) {
        this.isShowPagging = true;
      }
    });
  }
  changeCategory(event) {
    this.teacher.getListWorkShop(0, this.perPage, this.subjectCode, event).subscribe(data => {
      this.workshops = data.data;
      this.totalItems = data.totalElements;
      if (this.totalItems > this.perPage) {
        this.isShowPagging = true;
      }
    });
  }

  pageChanged(event: PageChangedEvent): void {
    this.teacher.getListWorkShop(event.page - 1, event.itemsPerPage).subscribe(data => {
      this.workshops = data.data;
      this.totalItems = data.totalElements;
      if (this.totalItems > this.perPage) {
        this.isShowPagging = true;
      }
    });
  }
}
