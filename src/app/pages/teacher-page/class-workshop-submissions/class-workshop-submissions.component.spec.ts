import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassWorkshopSubmissionsComponent } from './class-workshop-submissions.component';

describe('ClassWorkshopSubmissionsComponent', () => {
  let component: ClassWorkshopSubmissionsComponent;
  let fixture: ComponentFixture<ClassWorkshopSubmissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassWorkshopSubmissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassWorkshopSubmissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
