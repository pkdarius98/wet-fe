import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TeacherService } from 'src/app/services/teacher.service';
import { PageChangedEvent } from 'ngx-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-class-workshop-submissions',
  templateUrl: './class-workshop-submissions.component.html',
  styleUrls: ['./class-workshop-submissions.component.css']
})
export class ClassWorkshopSubmissionsComponent implements OnInit {

  workshopId: any;
  classId: any;
  susmissions: any[] = [];
  isShowPagging = false;
  totalItems: number;
  currentPage = 1;
  perPage = 10;
  maxSize = 5;
  constructor(
    private teacher: TeacherService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Class Submission | Workshop Evaluation');
    this.spinner.show();
    this.workshopId = this.route.snapshot.queryParams.workshopId;
    this.route.params.subscribe(params => {
      this.classId = params.classId;
    });
    this.teacher.getClassSubmission(this.classId, this.workshopId).subscribe(data => {
      this.susmissions = data.data;
      this.susmissions.forEach(item => {
        item.submitTime = new Date(item.submitTime * 1000).toDateString();
      });
      this.totalItems = data.totalElements;
      if (this.totalItems > this.perPage) {
        this.isShowPagging = true;
      }
      this.spinner.hide();
    });
  }

  pageChanged(event: PageChangedEvent): void {
    this.teacher.getListWorkShop(event.page - 1, event.itemsPerPage).subscribe(data => {
      this.susmissions = data.data;
      this.susmissions.forEach(item => {
        item.submitTime = new Date(item.submitTime * 1000).toDateString();
      });
      this.totalItems = data.totalElements;
      if (this.totalItems > this.perPage) {
        this.isShowPagging = true;
      }
    });
  }

}
