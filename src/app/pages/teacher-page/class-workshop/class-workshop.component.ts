import { Component, OnInit, ViewChild } from '@angular/core';
import { TeacherService } from 'src/app/services/teacher.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';
import Swal from 'sweetalert2';
import { ModalDirective, TabDirective } from 'ngx-bootstrap';
import * as XLSX from 'xlsx';
import { NgxSpinnerService } from 'ngx-spinner';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-class-workshop',
  templateUrl: './class-workshop.component.html',
  styleUrls: ['./class-workshop.component.css']
})
export class ClassWorkshopComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  private sub: any;
  workshops: any[] = [];
  listClassWorkshop: any[] = [];
  selectedWorkshop: any;
  title = 'Class Workshop';
  classId: number;
  isModalShown = false;
  scoreBoard: any;
  workshopList: any[] = [];
  students: any[] = [];
  scores: any;
  exportList: any[] = [];
  currentClass: any = {};
  classCode;
  subjectCode;
  swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-info',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  });
  gradeItems: any[] = [];
  selectedItem: any;

  constructor(
    private teacher: TeacherService,
    private route: ActivatedRoute,
    private admin: AdminService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private titleService: Title) { }

  ngOnInit(): void {
    this.spinner.show();
    this.sub = this.route.params.subscribe(params => {
      this.classId = params.classId;
      this.teacher.getClassWorkshop(this.classId, 0, 9999).subscribe(data => {
        this.spinner.hide();
        this.listClassWorkshop = data.data;
        if (data === []) {
          this.router.navigate(['/error']);
        }
      });
      this.teacher.getClasses().subscribe(data => {
        data.forEach((item:any) => {
          if (item.classId == this.classId) {
            this.currentClass = {
              classCode: item.classCode,
              semester: item.semester.name
            };
            this.classCode = item.classCode;
            this.subjectCode = item.subject.subjectCode;
            this.titleService.setTitle(this.subjectCode+' - '+this.classCode+' - Assign Workshop | Workshop Evaluation');
          }
        });
      });
      this.teacher.getScoreBoard(this.classId).subscribe(data => {
        this.spinner.hide();
        this.workshopList = [];
        this.exportList = [];
        this.scoreBoard = data;
        data.students.forEach( (value, i) => {
          const studentCv: any = {};
          studentCv.username = value.username;
          this.exportList.push(studentCv);
        });
        const scoree = Object.keys(data.scores);
        scoree.sort((a, b) => {
          const aIndex = +a.split(' ')[1];
          const bIndex = +b.split(' ')[1];
          if (aIndex < bIndex) {
            return -1;
          }
          return 1;
        });
        this.exportList.forEach( (value, i) => {
          scoree.forEach( ws => {
            value[`${ws}`] = data.scores[ws][i];
          });
        });
        scoree.forEach( ws => {
          this.workshopList.push(ws);
        });
        this.scores = data.scores;
        this.students = data.students;
      });
    });
  }

  onSelect(data1: TabDirective): void {
    if (data1.id === 'workshop') {
      this.spinner.show();
      this.teacher.getClassWorkshop(this.classId, 0, 9999).subscribe(data => {
        this.spinner.hide();
        this.listClassWorkshop = data.data;
        if (data === []) {
          this.router.navigate(['/error']);
        }
      });
    }
    if (data1.id === 'point') {
      this.workshopList = [];
      this.exportList = [];
      this.spinner.show();
      this.teacher.getScoreBoard(this.classId).subscribe(data => {
        this.spinner.hide();
        this.scoreBoard = data;
        data.students.forEach( (value, i) => {
          const studentCv: any = {};
          studentCv.username = value.username;
          this.exportList.push(studentCv);
        });
        const scoree = Object.keys(data.scores);
        scoree.sort((a, b) => {
          const aIndex = +a.split(' ')[1];
          const bIndex = +b.split(' ')[1];
          if (aIndex < bIndex) {
            return -1;
          }
          return 1;
        });
        this.exportList.forEach( (value, i) => {
          scoree.forEach( ws => {
            value[`${ws}`] = data.scores[ws][i];
          });
        });
        scoree.forEach( ws => {
          this.workshopList.push(ws);
        });
        this.scores = data.scores;
        this.students = data.students;
      });
    }
  }

  // remove(wshopId, index) {
  //   this.swalWithBootstrapButtons.fire({
  //     title: 'Are you sure?',
  //     text: 'Do you want to remove workshop?',
  //     showCancelButton: true,
  //     confirmButtonText: 'Yes, delete it!',
  //     cancelButtonText: 'No, cancel!'
  //   }).then((result) => {
  //     if (result.value) {
  //       this.teacher.actionToClassWorkshop(this.classId, wshopId, false)
  //         .subscribe(
  //           data => {
  //             this.listClassWorkshop.splice(index, 1);
  //             Swal.fire(
  //               'Deleted!',
  //               'Your testcase has been deleted.',
  //               'success'
  //             );
  //           });
  //     }
  //   });
  // }

  viewContent(index) {
    const pdfWindow = window.open('', '_blank');
    let html = '';
    html += '<html>';
    html += '<body style="margin:0!important">';
    html += '<embed width="100%" height="100%" src="data:application/pdf;base64,' + encodeURI(this.listClassWorkshop[index].content) + '" type="application/pdf" />';
    html += '</body>';
    html += '</html>';
    pdfWindow.document.write(html);
  }

  exportToExcel() {
      /* generate worksheet */
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.exportList);
      /* generate workbook and add the worksheet */
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, this.currentClass.classCode);
      /* save to file */
      XLSX.writeFile(wb, `${this.currentClass.semester}_${this.currentClass.classCode}.xlsx`);
  }

  addWorkshop() {
    if (!this.selectedWorkshop) {
      Swal.fire({
        title: 'Error',
        text: 'You must choose a workshop',
        type: 'error',
        confirmButtonClass: 'btn btn-error'
      });
      return;
    }

    if (!this.selectedItem) {
      Swal.fire({
        title: 'Error',
        text: 'You must choose a grade item',
        type: 'error',
        confirmButtonClass: 'btn btn-error'
      });
      return;
    }

    this.teacher.actionToClassWorkshop(this.classId, this.selectedWorkshop, this.selectedItem, true)
    .subscribe(
      data => {
        this.teacher.getUnImportWorkshop(this.classId).subscribe(it => {
          this.workshops = it;
        });
        this.listClassWorkshop.push(data);
        Swal.fire({
          title: 'Success!',
          text: 'Add workshop success',
          type: 'success',
          confirmButtonClass: 'btn btn-success'
        });
        this.hideModal();
      },
      error => {
          Swal.fire({
            title: error.error.apierror.status,
            text: error.error.apierror.message,
            type: 'error'
          });
      }
      );
  }

  showModal(): void {
    this.teacher.getUnImportWorkshop(this.classId).subscribe(data => {
      this.workshops = data;
    });
    this.teacher.getUnAssignedGradeItems(this.classId).subscribe(data => {
      this.gradeItems = data;
    });
    this.isModalShown = true;
  }
  onHidden(): void {
    this.isModalShown = false;
    this.selectedWorkshop = '';
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

}
