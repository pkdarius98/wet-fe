import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassWorkshopComponent } from './class-workshop.component';

describe('ClassWorkshopComponent', () => {
  let component: ClassWorkshopComponent;
  let fixture: ComponentFixture<ClassWorkshopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassWorkshopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassWorkshopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
