import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassWorkshopSubmissionsDetailComponent } from './class-workshop-submissions-detail.component';

describe('ClassWorkshopSubmissionsDetailComponent', () => {
  let component: ClassWorkshopSubmissionsDetailComponent;
  let fixture: ComponentFixture<ClassWorkshopSubmissionsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassWorkshopSubmissionsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassWorkshopSubmissionsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
