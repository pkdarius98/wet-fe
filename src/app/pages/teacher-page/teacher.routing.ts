import { SubmissionDetailComponent } from './submission-detail/submission-detail.component';
import { SubmissionComponent } from './submission/submission.component';
import { Routes } from '@angular/router';

import { WorkshopDetailComponent } from './workshop-detail/workshop-detail.component';
import { WorkshopsComponent } from './workshops/workshops.component';
import { WorkshopTestcasesComponent } from './workshop-testcases/workshop-testcases.component';
import { WorkshopAddComponent } from './workshop-add/workshop-add.component';
import { WorkshopEditComponent } from './workshop-edit/workshop-edit.component';
import { ClassWorkshopComponent } from './class-workshop/class-workshop.component';
import { ClassWorkshopSubmissionsComponent } from './class-workshop-submissions/class-workshop-submissions.component';
import { ClassWorkshopSubmissionsDetailComponent } from './class-workshop-submissions-detail/class-workshop-submissions-detail.component';

export const TeacherRoutes: Routes = [
      {
        path: 'workshops',
        component: WorkshopsComponent
      },
      {
        path: 'workshops/add',
        component: WorkshopAddComponent
      },
      {
        path: 'workshops/:id/edit',
        component: WorkshopEditComponent
      },
      {
        path: 'workshops/:id',
        component: WorkshopDetailComponent,
      },
      {
        path: 'workshops/:id/testcases',
        component: WorkshopTestcasesComponent
      },
      {
        path: 'submission',
        component: SubmissionComponent
      },
      {
        path: 'submission-detail/:id',
        component: SubmissionDetailComponent
      },
      {
        path: 'class/:classId',
        component: ClassWorkshopComponent
      },
      {
        path: 'class/:classId/submissions',
        component: ClassWorkshopSubmissionsComponent
      },
      {
        path: 'class/:classId/submissions/:submitId',
        component: ClassWorkshopSubmissionsDetailComponent
      }
];
