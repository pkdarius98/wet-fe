import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopTestcasesComponent } from './workshop-testcases.component';

describe('WorkshopTestcasesComponent', () => {
  let component: WorkshopTestcasesComponent;
  let fixture: ComponentFixture<WorkshopTestcasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshopTestcasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopTestcasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
