import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TeacherService } from '../../../services/teacher.service';
import { TestCase, TestcaseZipFile } from '../../../model/models';
import { Observable } from 'rxjs';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { LocalStorageService } from 'ngx-webstorage';
import { ModalDirective } from 'ngx-bootstrap';
import { bounceIn } from 'ngx-animate';
import { trigger, transition, useAnimation } from '@angular/animations';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer, SafeResourceUrl, SafeUrl, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-workshop-testcases',
  templateUrl: './workshop-testcases.component.html',
  styleUrls: ['./workshop-testcases.component.css'],
  animations: [
    trigger('bounceIn', [transition(':enter', useAnimation(bounceIn))]),
    trigger('bounceIn', [transition(':leave', useAnimation(bounceIn))])
  ]
})
export class WorkshopTestcasesComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  bounceIn: any;
  testCase$: Observable<TestCase[]>;
  testCases: TestCase[] = [];
  testCase: TestCase = {};
  isChecked = false;
  workshopId: any;
  base64In: any;
  base64Out: any;
  base64Img: any;
  div: any;
  preValue: any = {};
  tempValue: TestCase;
  focusTouched;
  focusTouched1;
  focusTouched2;
  isValidFormSubmitted = false;

  testcaseForm = this.fb.group({
    input: ['', Validators.required],
    output: ['', Validators.required],
    point: [0, Validators.required],
    sample: [false],
  });

  uploading = false;
  fileList: NzUploadFile[] = [];
  fileBase64: TestcaseZipFile;
  isModalShown = false;
  isDarkmode = this.getDashBoardColor() === 'white-content' ? false : true;
  swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-info',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  });

  constructor(
    private route: ActivatedRoute,
    private teacher: TeacherService,
    private fb: FormBuilder,
    private msg: NzMessageService,
    private localStorage: LocalStorageService,
    private spinner: NgxSpinnerService,
    private sanitizer: DomSanitizer,
    private titleService: Title) {

  }

  ngOnInit(): void {
    this.titleService.setTitle('Workshop Testcases | Workshop Evaluation');
    this.route.params.subscribe(params => {
      this.workshopId = params.id;
      this.spinner.show();
      this.teacher.getTestCaseById(this.workshopId)
        .subscribe(testcases => {
          this.testCases = testcases;
          this.testCases.forEach(testcase => {
            testcase.input = 'data:text/html;base64,' + testcase.input;
            testcase.output = 'data:text/html;base64,' + testcase.output;
          });
          this.spinner.hide();
        });
    });
    this.div = document.getElementById('div');
  }

  onSubmitTc(){
    this.isValidFormSubmitted = true;
    if (!this.testcaseForm.valid) {
      return;
    }
    const obj = this.testcaseForm.value;
    obj.input = this.testCase.input;
    obj.output = this.testCase.output;
    obj.workshopId = this.workshopId;
    this.teacher.addTestCase(obj)
      .subscribe(
        data => {
          data.input = 'data:text/html;base64,' + data.input;
          data.output = 'data:text/html;base64,' + data.output;
          this.testCases.push(data);
          Swal.fire({
            title: 'Success!',
            text: 'Add Testcase Success',
            type: 'success',
            confirmButtonClass: 'btn btn-success'
          });
          this.hideModal();
        }
      );
  }

  download(id, index) {
    const downloadLink = document.createElement('a');
    const fileName = `${id}.w${index + 1}.in`;
    downloadLink.href = this.testCases[index].input;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  handleCharacterE(e) {
    return e.keyCode !== 69;
  }

  showModal(): void {
    this.isModalShown = true;
    this.isDarkmode = this.getDashBoardColor() === 'white-content' ? false : true;
  }

  getDashBoardColor() {
    return this.localStorage.retrieve('dashColor');
  }

  onHidden(): void {
    this.isModalShown = false;
    this.testCase = {} as TestCase;
    this.isValidFormSubmitted = false;
    this.resetForm(this.testcaseForm);
    this.testcaseForm.controls.point.setValue(0);
    this.testcaseForm.controls.sample.setValue(false);
  }

  get testCaseF() {
    return this.testcaseForm.controls;
  }

  resetForm(form: FormGroup) {
    form.reset();
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

  onSubmit() {
    this.testCases.forEach(testcase => {
      if (testcase.input.includes('data:text/html;base64,')) {
        testcase.input = testcase.input.split(',')[1];
      }
      if (testcase.output.includes('data:text/html;base64,')) {
        testcase.output = testcase.output.split(',')[1];
      }
    });
    this.teacher.editTestCase(this.testCases, this.workshopId)
      .subscribe(
        data => {
          this.testCases = data;
          this.testCases.forEach(testcase => {
            testcase.input = 'data:text/html;base64,' + testcase.input;
            testcase.output = 'data:text/html;base64,' + testcase.output;
          });
          Swal.fire({
            title: 'Success!',
            text: 'Update Testcase Success',
            type: 'success',
            confirmButtonClass: 'btn btn-success'
          });
        }
      );
  }

  handleFileInput(event: { target: { files: any[]; }; }, index) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.base64In = reader.result;
      console.log(this.base64In.split(',')[1]);
      this.testCases[index].input = this.base64In.split(',')[1];
    };
    const x = document.getElementById(`update_input${index}`);
    x.innerHTML = file.name;
  }

  handleFileInput2(event: { target: { files: any[]; }; }, index) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.base64Out = reader.result;
      this.testCases[index].output = this.base64Out.split(',')[1];
    };
    const x = document.getElementById(`update_output${index}`);
    x.innerHTML = file.name;
  }

  handleFileInput3(event: { target: { files: any[]; }; }, index) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.base64Img = reader.result;
      this.testCases[index].image = this.base64In.split(',')[1];
      this.testCases[index].imageType = file.name.split('.')[1];
    };
    const x = document.getElementById(`update_image${index}`);
    x.innerHTML = file.name;
  }

  handleFileInput4(event: { target: { files: any[]; }; }) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const dataInp: any = reader.result;
      this.testCase.input = dataInp.split(',')[1];
    };
    const x = document.getElementById(`new_input`);
    x.innerHTML = file.name;
  }

  handleFileInput5(event: { target: { files: any[]; }; }) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const dataInp: any = reader.result;
      this.testCase.output = dataInp.split(',')[1];
    };
    const x = document.getElementById(`new_output`);
    x.innerHTML = file.name;
  }

  handleFileInput6(event: { target: { files: any[]; }; }) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const dataInp: any = reader.result;
      this.testCase.image = dataInp.split(',')[1];
      this.testCase.imageType = file.type;
    };
    const x = document.getElementById(`new_image`);
    x.innerHTML = file.name;
  }

  handleEdit(index) {
    const button = document.getElementById(`btn-click${index}`);
    const textarea = document.getElementById(`textarea${index}`);
    const span = document.getElementById(`description${index}`);
    if (button.textContent.trim() === 'Edit') {
      this.preValue[index] = this.testCases[index].point;
      span.style.display = 'none';
      textarea.style.display = 'block';
      textarea.innerText = `${this.testCases[index].point}`;
      button.innerText = 'Cancel';
    }
    else {
      this.testCases[index].point = this.preValue[index];
      delete this.preValue[index];
      span.style.display = 'inline';
      textarea.style.display = 'none';
      button.innerText = 'Edit';
    }
  }

  up(id, index) {
    if (this.testCases[0] !== this.testCases[index]) {

      this.teacher.upRank(this.workshopId, id).subscribe(
        data => data,
        error => {
          this.msg.error(error.error());
        },
        () => {
          const temp = this.testCases[index].rank;
          this.testCases[index].rank = this.testCases[index - 1].rank;
          this.testCases[index - 1].rank = temp;
          this.testCases.sort(this.compare);
        }
      );
    }
    return;
  }

  down(id, index) {
    const lenght = this.testCases.length;
    if (this.testCases[lenght - 1] !== this.testCases[index]) {
      this.teacher.downRank(this.workshopId, id).subscribe(
        data => data,
        error => {
          this.msg.error(error.error());
        },
        () => {
          const temp = this.testCases[index].rank;
          this.testCases[index].rank = this.testCases[index + 1].rank;
          this.testCases[index + 1].rank = temp;
          this.testCases.sort(this.compare);
        }
      );
    }
    return;
  }

  remove(id, i) {
    this.swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: 'Do you want to remove this testcase',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!'
    }).then((result) => {
      if (result.value) {
        this.teacher.deleteTestCase(this.workshopId, id)
          .subscribe(
            data => {
              this.testCases.splice(i, 1);
              Swal.fire(
                'Deleted!',
                'Your testcase has been deleted.',
                'success'
              );
            },
            error => {
              Swal.fire({
                title: error.error.apierror.status,
                text: error.error.apierror.message,
                type: 'error'
              });
            }
          );
      }
    });
  }

  compare(a: TestCase, b: TestCase) {
    if (a.rank < b.rank) {
      return -1;
    }
    if (a.rank > b.rank) {
      return 1;
    }
    return 0;
  }

  beforeUpload = (file): boolean => {
    this.fileList = [];
    this.fileList = this.fileList.concat(file);
    const reader = new FileReader();
    reader.readAsDataURL(file);
    if (file.name.substring(file.name.lastIndexOf('.')) !== '.zip') {
      this.fileList[0].status = 'error';
      this.fileList[0].response = 'Only accept .zip type';
      this.msg.error('Only accept .zip type');
    } else {
      reader.onload = () => {
        const bs64: any = reader.result;
        this.fileBase64 = {
          data: bs64.split(',')[1],
          fileName: file.name
        };
      };
    }
    return false;
  }

  handleUpload(): void {
    this.uploading = true;
    this.teacher.uploadTestCase(this.fileBase64, this.workshopId)
      .subscribe(
        data => {
          this.testCases = data;
          this.testCases.forEach(testcase => {
            testcase.input = 'data:text/html;base64,' + testcase.input;
            testcase.output = 'data:text/html;base64,' + testcase.output;
          });
          Swal.fire({
            title: 'Success!',
            text: 'Upload file success',
            type: 'success',
            confirmButtonClass: 'btn btn-success'
          });
          this.uploading = false;
          this.fileList = [];
          this.fileBase64 = {} as TestcaseZipFile;
        },
        error => {
          this.uploading = false;
          Swal.fire({
            title: error.error.apierror.status,
            text: error.error.apierror.message,
            type: 'error'
          });
        }
      );
  }

}
