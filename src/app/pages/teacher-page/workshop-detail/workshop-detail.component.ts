import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TeacherService } from 'src/app/services/teacher.service';
import { Observable } from 'rxjs';
import { Workshop, TestCase } from '../../../model/models';
import { AdminService } from '../../../services/admin.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-workshop-detail',
  templateUrl: './workshop-detail.component.html',
  styleUrls: ['./workshop-detail.component.css']
})
export class WorkshopDetailComponent implements OnInit {

  private sub: any;
  workshopID: any;
  workshop$: Observable<Workshop>;
  workshop: Workshop;
  constructor(
    private teacher: TeacherService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private titleService: Title
    ) { }

  ngOnInit(): void {
    this.titleService.setTitle('Workshop Detail | Workshop Evaluation');
    this.spinner.show();
    this.sub = this.route.params.subscribe(params => {
      this.workshopID = params.id;
      this.teacher.getWorkshopById(this.workshopID).subscribe(data => {
        this.spinner.hide();
        this.workshop = data;
      });
    });
  }

}
