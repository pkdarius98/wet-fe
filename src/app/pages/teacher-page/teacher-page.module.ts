import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TeacherRoutes } from './teacher.routing';

import { WorkshopDetailComponent } from './workshop-detail/workshop-detail.component';
import { WorkshopsComponent } from './workshops/workshops.component';
import { WorkshopTestcasesComponent } from './workshop-testcases/workshop-testcases.component';
import { WorkshopAddComponent } from './workshop-add/workshop-add.component';
import { WorkshopEditComponent } from './workshop-edit/workshop-edit.component';
import { HttpErrorInterceptor } from '../../interceptors/http-error.interceptor';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClientJsonpModule} from '@angular/common/http';
import { ComponentModule } from '../../component/component.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TagInputModule } from 'ngx-chips';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { SubmissionComponent } from './submission/submission.component';
import { SubmissionDetailComponent } from './submission-detail/submission-detail.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { ClassWorkshopComponent } from './class-workshop/class-workshop.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ClassWorkshopSubmissionsComponent } from './class-workshop-submissions/class-workshop-submissions.component';
import { ClassWorkshopSubmissionsDetailComponent } from './class-workshop-submissions-detail/class-workshop-submissions-detail.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { SafePipeModule } from 'safe-pipe';

@NgModule({
  declarations: [
    WorkshopDetailComponent,
    WorkshopsComponent,
    WorkshopTestcasesComponent,
    WorkshopAddComponent,
    WorkshopEditComponent,
    SubmissionComponent,
    SubmissionDetailComponent,
    ClassWorkshopComponent,
    ClassWorkshopSubmissionsComponent,
    ClassWorkshopSubmissionsDetailComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(TeacherRoutes),
    ModalModule.forRoot(),
    ComponentModule,
    FormsModule,
    ReactiveFormsModule,
    TagInputModule,
    NzSelectModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    NzUploadModule,
    NzButtonModule,
    NzMessageModule,
    NzTagModule,
    NzIconModule,
    NgxSpinnerModule,
    NzEmptyModule,
    SafePipeModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    }
  ]
})
export class TeacherPageModule { }
