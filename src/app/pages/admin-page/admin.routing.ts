import { LanguageComponent } from './language/language.component';
import { ClassesDetailComponent } from './classes/classes-detail/classes-detail.component';
import { Routes } from '@angular/router';

import { TeachersComponent } from './teachers/teachers.component';
import { StudentsComponent } from './students/students.component';
import { ClassesComponent } from './classes/classes.component';
import { SemesterComponent } from './semester/semester.component';
import { ExecutableComponent } from './executable/executable.component';
import { ExecutableDetailsComponent } from './executable-details/executable-details.component';
import { SubjectsComponent } from './subjects/subjects.component';

export const StudentRoutes: Routes = [
  {
    path: 'semester',
    component: SemesterComponent
  },
  {
    path: 'classes',
    component: ClassesComponent
  },
  {
    path: 'classes-detail/:id',
    component: ClassesDetailComponent
  },
  {
    path: 'students',
    component: StudentsComponent
  },
  {
    path: 'teachers',
    component: TeachersComponent
  },
  {
    path: 'executable',
    component: ExecutableComponent
  },
  {
    path: 'executable/:id',
    component: ExecutableDetailsComponent
  },
  {
    path: 'languages',
    component: LanguageComponent
  },
  {
    path: 'subjects',
    component: SubjectsComponent
  }
];
