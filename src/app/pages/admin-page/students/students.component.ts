import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { forEach } from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';
import { AdminService } from './../../../services/admin.service';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { debounceTime, distinctUntilChanged, switchMap, filter, tap } from 'rxjs/operators';
import swal from 'sweetalert2/src/sweetalert2.js'
@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  @ViewChild('autoShownModalEdit', { static: false }) autoShownModalEdit: ModalDirective;
  @ViewChild('fullName') fullName: ElementRef;
  @ViewChild('userName') userName: ElementRef;
  @ViewChild('rollNumber') rollNumber: ElementRef;
  @ViewChild('email') email: ElementRef;
  @ViewChild('class') class: ElementRef;
  @ViewChild('getRollNumber') getRollNumber: ElementRef;
  isModalShown = false;
  isModalShownEdit = false;
  focusTouched;
  focusTouched1;
  focusTouched2;
  focusTouched3;
  focusTouched4;
  focusTouched5;
  focusTouched6;
  focusTouched7;
  focusTouched8;
  focusTouched9;
  focusTouched10;
  isValidFormSubmitted = false;
  Student$: Observable<any>;
  listClass = [];
  listClassDetail = [];
  data: string;
  StudentTable$: Observable<any[]>;
  StudentTable1$: Observable<any[]>;
  Semester$: Observable<any[]>;
  listStudent: any;
  listStudentDetail: any;
  listStudentOnSearch = [];
  listStudentEdit: any;
  page = 1;
  totalSize: number;
  returnedArray: string[];
  listStudentAll: any;
  getSemester: any = [];
  getSemesterCheck = [];
  searchBySemester: string = "";
  searchByStatus: string = "";
  getStatus = ['All Status', 'Active', 'De-Active'];
  isPageShow: boolean = false;
  private search = new Subject<any>();
  selectedProvinceSemester = 'All Semester';
  selectedProvinceStatus = 'Active';
  maxSize = 5;
  constructor(private fb: FormBuilder,
    private adminService: AdminService
    , private router: Router,
    private localStorage: LocalStorageService,
    private spinner: NgxSpinnerService,
    private titleService: Title
  ) {
  }

  studentForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    fullName: ['', [Validators.required]],
    rollNumber: ['', [Validators.required]],
    userName: ['', [Validators.required]],
  });
  studentEditForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    fullName: ['', [Validators.required]],
    rollNumber: ['', [Validators.required]],
    userName: ['', [Validators.required]],
    status: ['', [Validators.required]],
    classes: ['']
  });
  ngOnInit(): void {
    this.titleService.setTitle('Students | Workshop Evaluation');
    this.getAllStudent();
    this.getSemesterCheck.push("All Semester");
    this.getAllSemester();
  }

  searchTerm(term: string) {
    this.search.next(term);
  }

  searchStudent() {
    this.StudentTable$ = this.search.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => { this.spinner.show() }),
      switchMap((term: string) =>
        this.adminService.searchStudent(this.page, term, this.searchBySemester, this.searchByStatus)
      )
    )
    this.StudentTable$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.enabled = item.enabled ? 1 : 0;
      })
      this.listStudentOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
  }

  getAllSemester() {
    this.Semester$ = this.adminService.searchSemester(this.page, "", "");
    this.Semester$.subscribe((data: any[]) => {

      this.getSemester = data[2];
      this.getSemester.forEach(item => {
        this.getSemesterCheck.push(item.name);
      })
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
  }
  get_data(event) {
    console.log(event);
    if (event == "All Semester") {
      this.searchBySemester = "";
    } else {
      this.searchBySemester = event;
    }
    this.spinner.show();
    this.StudentTable$ = this.adminService.searchStudent(this.page, "", this.searchBySemester, this.searchByStatus);
    this.StudentTable$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.enabled = item.enabled ? 1 : 0;
      })
      this.listStudentOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
  }
  getStatusData(event) {
    if (event == "Active") {
      this.searchByStatus = '';
    } else if (event == "All Status") {
      this.searchByStatus = '____';
    }
    else {
      this.searchByStatus = '___0';
    }
    this.spinner.show();
    this.StudentTable$ = this.adminService.searchStudent(this.page, "", this.searchBySemester, this.searchByStatus);
    this.StudentTable$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.enabled = item.enabled ? 1 : 0;
      })
      this.listStudentOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
  }
  getAllStudent() {
    this.listStudent = [];
    this.spinner.show();
    this.StudentTable$ = this.adminService.searchStudent(this.page, "", this.searchBySemester, this.searchByStatus);
    this.StudentTable$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.enabled = item.enabled ? 1 : 0;
      })
      this.listStudent = data[2];
      this.listStudentOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
    this.searchStudent();
  }
  showModal(): void {
    this.isModalShown = true;
  }
  onHidden(): void {
    this.isModalShown = false;
    this.isValidFormSubmitted = false;
    this.resetForm(this.studentForm);
  }
  hideModal(): void {
    this.autoShownModal.hide();
  }
  onSubmit() {
    this.isValidFormSubmitted = true;
    if (!this.studentForm.valid) {
      return;
    }
    const student = {
      email: this.studentForm.value.email,
      fullName: this.studentForm.value.fullName,
      rollNumber: this.studentForm.value.rollNumber,
      username: this.studentForm.value.userName
    }
    this.adminService.addStudent(student)
      .subscribe(
        data => console.log(data)
        , error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        },
        () => {
          swal.fire({
            title: 'Success!',
            text: 'Create student success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(() => {
            this.hideModal();
            this.getAllStudent();
          });


        }
      );
  }
  get studentF() {
    return this.studentForm.controls;
  }
  get studentEditF() {
    return this.studentEditForm.controls;
  }
  resetForm(form: FormGroup) {
    form.reset();
  }
  onKey(event) {
    this.data = event.target.value;
    //this.studentForm.get('email').setValue(this.data.substring(0, this.data.lastIndexOf('@')+1)+'@fpt.edu.vn');
    this.studentForm.get('email').setValue(this.data + '@fpt.edu.vn');
  }
  showDetail(index) {
    this.listStudentDetail = this.listStudentOnSearch[index];
    this.router.navigate([`/students/student-detail/${this.listStudentDetail.rollNumber}`]);
  }
  Active(index) {
    swal.fire({
      title: 'Are you sure to activate student?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.adminService.activeStudent(this.listStudentOnSearch[index].rollNumber).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Activate student success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(() => {
            this.getAllStudent();
          });
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        })
      }
    })
  }
  deActive(index) {
    swal.fire({
      title: 'Are you sure to deactivate student?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.adminService.deactiveStudent(this.listStudentOnSearch[index].rollNumber).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Deactivate student success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(() => {
            this.getAllStudent();
          });
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        })
      }
    })
  }
  showModalEdit(index): void {
    let student = [];
    let studentDetail = [];
    this.isModalShownEdit = true;
    let statusConvert: any;

    this.listStudentEdit = this.listStudentOnSearch[index];
    student = this.listStudentEdit.studentClasses;
    student.forEach(element => {
      studentDetail.push(element.classCode + " - " + element.subject.subjectCode + "\n");
    })
    let flag = studentDetail.toString();
    flag = flag.split(',').join('');

    statusConvert = this.listStudentEdit.enabled;
    statusConvert = statusConvert ? "Active" : "De-Active";
    this.studentEditForm.patchValue({
      email: this.listStudentEdit.email,
      fullName: this.listStudentEdit.fullName,
      rollNumber: this.listStudentEdit.rollNumber,
      userName: this.listStudentEdit.username,
      status: statusConvert,
      classes: flag
    })

  }
  onHiddenEdit(): void {
    this.isModalShownEdit = false;
    this.isValidFormSubmitted = false;
    this.resetForm(this.studentEditForm);
  }
  hideModalEdit(): void {
    this.autoShownModalEdit.hide();
  }
  onSubmitEdit() {
    this.isValidFormSubmitted = true;
    if (!this.studentEditForm.valid) {
      return;
    }
    const student = {
      email: this.studentEditForm.value.email,
      fullName: this.studentEditForm.value.fullName,
      rollNumber: this.studentEditForm.value.rollNumber,
      username: this.studentEditForm.value.userName
    }
    this.adminService.updateStudent(student, this.listStudentEdit.rollNumber)
      .subscribe(
        data => console.log(data)
        , error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        },
        () => {
          swal.fire({
            title: 'Success!',
            text: 'Update student success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(() => {
            this.hideModalEdit();
            this.getAllStudent();
          });
        }
      );
  }
  pageChanged(event: any): void {
    this.getAllStudent();
  }
}
