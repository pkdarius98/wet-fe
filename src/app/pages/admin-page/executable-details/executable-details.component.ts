import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Executable } from '../../../model/models';
import { Observable } from 'rxjs';
import { AdminService } from '../../../services/admin.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { ModalDirective } from 'ngx-bootstrap/modal';
import swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-executable-details',
  templateUrl: './executable-details.component.html',
  styleUrls: ['./executable-details.component.css']
})
export class ExecutableDetailsComponent implements OnInit, OnDestroy {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  @ViewChild('fileInput') fileInput: ElementRef;
  isModalShown = false;
  focusTouched1 = false;
  isValidFormSubmitted = false;
  executables$: Observable<Executable[]>;
  uploadedFiles: File;
  fileBase64: any;
  filesName = '';
  fileSize: any;
  isChoosenFile = false;
  excutableForm = this.fb.group({
    types: ['', Validators.required],
    description: [''],
    files: [null]
  });
  subjects = [
    { id: '0', itemName: 'compile' },
    { id: '1', itemName: 'run' },
    { id: '2', itemName: 'compare' }
  ];
  selectedSubject: any;

  private executableId: string;
  executable$: Observable<Executable>;
  executable: Executable;
  private sub: any;
  downloadUrl: string;
  fileName: string;

  constructor(
    private admin: AdminService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private spinner: NgxSpinnerService,
    private titleService: Title) {}

  ngOnInit(): void {
    this.titleService.setTitle('Executable Detail | Workshop Evaluation');
    this.sub = this.route.params.subscribe(params => {
      this.spinner.show();
      this.executableId = params.id;
    });
    this.admin.getExecutableById(this.executableId).subscribe(data => {
      this.spinner.hide();
      this.executable = data;
      this.downloadUrl = 'data:application/zip;base64,' + this.executable.zipFileBase64;
      this.fileName = this.executable.executableId + '.zip';
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  handleFileInput(event: { target: { files: any[]; }; }) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    if (file.name.substring(file.name.lastIndexOf(".")) !== ".zip") {
      this.fileC.setValue('');
      swal.fire({
        title: 'Invalid file extension',
        text: 'Only accept zip type',
        type: 'error',
      });
    } else {
      reader.onload = () => {
        this.fileBase64 = reader.result;
      };
      this.uploadedFiles = file;
      this.filesName = file.name;
      this.isChoosenFile = true;
    }
  }

  fileChoosen() {
    this.fileInput.nativeElement.click();
  }

  get executeF() {
    return this.excutableForm.controls;
  }

  get fileC() {
    return this.excutableForm.get('files');
  }

  get descriptC() {
    return this.excutableForm.get('description');
  }

  resetForm(form: FormGroup) {
    form.reset();
  }

  showModal(executable: Executable): void {
    this.isModalShown = true;
    this.descriptC.setValue(executable.description);
    this.subjects.forEach(item => {
      if (executable.type === item.itemName) {
        this.selectedSubject = item.itemName;
      }
    });
  }

  onHidden(): void {
    this.isModalShown = false;
    this.filesName = '';
  }

  hideModal(): void {
    this.autoShownModal.hide();
    console.log(this.isModalShown);
  }

  download() {
    const downloadLink = document.createElement('a');
    const fileName = this.fileName;
    console.log(this.downloadUrl);
    downloadLink.href = this.downloadUrl;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  onSubmit(executable: Executable) {
    this.isValidFormSubmitted = true;
    if (!this.excutableForm.valid) {
      return;
    }
    executable.description = this.descriptC.value;
    executable.type = this.excutableForm.value.types;
    if (this.isChoosenFile) {
      executable.zipFileBase64 = this.fileBase64.split(',')[1];
      this.isChoosenFile = false;
    }
    this.admin.editExecutable(executable)
    .subscribe(
      data => data
    , error => {
      swal.fire({
        title: error.error.apierror.status,
        text: error.error.apierror.message,
        type: 'error',
      });
    },
    () => {
      swal.fire({
        title: 'Success!',
        text: 'Edit executable success',
        type: 'success'
      }).then(result => {
        if (result.value) {
          this.hideModal();
          this.isModalShown = false;
          this.admin.getExecutableById(this.executableId).subscribe(data => {
            this.executable = data;
            this.downloadUrl = 'data:application/zip;base64,' + this.executable.zipFileBase64;
            this.fileName = this.executable.executableId + '.zip';
          });
        }
      });
    }
    );
  }

}
