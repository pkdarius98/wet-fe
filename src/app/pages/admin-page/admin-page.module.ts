import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StudentRoutes } from './admin.routing';

import { TeachersComponent } from './teachers/teachers.component';
import { StudentsComponent } from './students/students.component';
import { ClassesComponent } from './classes/classes.component';
import { SemesterComponent } from './semester/semester.component';
import { ExecutableComponent } from './executable/executable.component';
import { ExecutableDetailsComponent } from './executable-details/executable-details.component';
import { ClassesDetailComponent } from './classes/classes-detail/classes-detail.component';

import { ModalModule } from 'ngx-bootstrap/modal';
import { ComponentModule } from '../../component/component.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { PaginationModule } from "ngx-bootstrap/pagination";
import { TooltipModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LanguageComponent } from './language/language.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { NzEmptyModule } from 'ng-zorro-antd/empty';

@NgModule({
  declarations: [
    TeachersComponent,
    StudentsComponent,
    ClassesComponent,
    SemesterComponent,
    ExecutableComponent,
    ExecutableDetailsComponent,
    ClassesDetailComponent,
    LanguageComponent,
    SubjectsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(StudentRoutes),
    ModalModule.forRoot(),
    ComponentModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    PaginationModule.forRoot(),
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    NzSelectModule,
    NgxSpinnerModule,
    NzEmptyModule
  ]
})
export class AdminPageModule { }
