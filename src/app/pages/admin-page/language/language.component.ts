import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';
import { forEach } from 'lodash';
import { AdminService } from 'src/app/services/admin.service';
import { Observable } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import swal from 'sweetalert2/src/sweetalert2.js'
@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.css']
})
export class LanguageComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  isModalShown = false;
  listLanguage = [];
  listNoLanguage = [];
  page = 1;
  totalSize: number;
  isPageShow: boolean = false;
  Language$: Observable<any[]>;
  noLanguage$: Observable<any[]>;
  selectedProvince = null;
  selectedProvincePoint = null
  isValidFormSubmitted = false;
  focusTouched;
  focusTouched1;
  focusTouched2;
  focusTouched3;
  focusTouched4;
  listEntryPoint = ['True', 'False'];
  compileScriptId: string;
  requireEntryPoint: string;
  maxSize = 5;
  constructor(private adminService: AdminService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Languages | Workshop Evaluation');
    this.getAllLanguage();
  }
  getAllLanguage() {
    this.spinner.show();
    this.Language$ = this.adminService.getLanguage(this.page);
    this.Language$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach((item:any) =>{
        item.extensions = item.extensions.join(', ');
      })
      this.listLanguage = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
    });
  }
  getAllNoLanguage() {
    this.noLanguage$ = this.adminService.getNoLanguage();
    this.noLanguage$.subscribe((data: any) => {
      this.listNoLanguage = data;
    })
  }
  provinceChange(value: any): void {
    this.compileScriptId = value;
  }
  provinceChangePoint(value: any): void {
    this.requireEntryPoint = value;
  }
  submitForm = this.fb.group({
    compileScript: [null, Validators.required],
    extensions: ['', Validators.required],
    languageId: ['', Validators.required],
    name: ['', Validators.required],
    requireEntryPoint: [null, Validators.required]
  });
  get submitF() {
    return this.submitForm.controls;
  }
  pageChanged(event: any): void {
    this.getAllLanguage();
  }
  resetForm(form: FormGroup) {
    form.reset();
  }
  showModal(): void {
    this.isModalShown = true;
    this.getAllNoLanguage();
  }

  onHidden(): void {
    this.isModalShown = false;
    this.isValidFormSubmitted = false;
    this.resetForm(this.submitForm);
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

  onSubmit() {
    this.isValidFormSubmitted = true;
    if (!this.submitForm.valid) {
      return;
    }
    const addLanguage = {
      compileScriptId: this.compileScriptId,
      extensions: this.submitForm.value.extensions.split(','),
      languageId: this.submitForm.value.languageId,
      name: this.submitForm.value.name,
      requireEntryPoint: this.requireEntryPoint
    }
    this.adminService.addLanguage(addLanguage)
      .subscribe(
        data => console.log(data)
        , error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        },
        () => {
          swal.fire({
            title: 'Success!',
            text: 'Create language success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.hideModal();
            this.getAllLanguage();
          });
        }
      );
  }

}
