import { Title } from '@angular/platform-browser';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AdminService } from 'src/app/services/admin.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.css']
})
export class SubjectsComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;

  isValidFormSubmitted = false;
  isModalShown = false;
  languages: any[] = [];
  focusTouched;
  focusTouched1;
  selectedLanguage: any;
  subjects: any[] = [];
  submitForm = this.fb.group({
    subjectCode: ['', Validators.required],
    language_id: ['', Validators.required],
  });

  constructor(private admin: AdminService, private spinner: NgxSpinnerService, private fb: FormBuilder,private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Subjects | Workshop Evaluation');
    this.spinner.show();
    this.admin.getSubjects().subscribe(data => {
      this.spinner.hide();
      this.subjects = data;
    });
  }

  resetForm(form: FormGroup) {
    form.reset();
  }

  onSubmit() {
    this.isValidFormSubmitted = true;
    if (!this.submitForm.valid) {
      return;
    }
    this.admin.createSubject(this.submitForm.value)
    .subscribe(
      data => {
        Swal.fire({
          title: 'Success!',
          text: 'Create subject success',
          type: 'success',
          confirmButtonClass: 'btn btn-success'
        });
        this.hideModal();
        this.subjects.push(data);
      }
    );
  }

  get submitF() {
    return this.submitForm.controls;
  }

  showModal(): void {
    this.admin.getLanguages().subscribe(data => {
      this.languages = data;
    });
    this.isModalShown = true;
  }
  onHidden(): void {
    this.isModalShown = false;
    this.isValidFormSubmitted = false;
    this.resetForm(this.submitForm);
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

}
