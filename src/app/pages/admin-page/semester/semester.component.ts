import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalStorageService } from 'ngx-webstorage';
import { AdminService } from './../../../services/admin.service';
import { Observable, Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import swal from 'sweetalert2/src/sweetalert2.js'
import { Component, OnInit, ViewChild } from '@angular/core';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-semester',
  templateUrl: './semester.component.html',
  styleUrls: ['./semester.component.css']
})
export class SemesterComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  @ViewChild('autoShownEdit', { static: false }) autoShownEdit: ModalDirective;
  isModalShown = false;
  isEditShown = false;
  listSemester = [];
  isValidFormSubmitted = false;
  focusTouched;
  focusTouched1;
  focusTouched2;
  focusTouched3;
  focusTouched4;
  focusTouched5;
  listSemesterEdit: any;
  Semester$: Observable<any[]>;
  Semester1$: Observable<any[]>;
  page = 1;
  totalSize: number;
  listSemesterAll: any;
  listSemesterOnSearch = [];
  searchByActivated: string = "";
  activated = ['All Status', 'Active', 'De-Active'];
  isPageShow: boolean = false;
  selectedProvince = 'All Status';
  maxSize = 5;
  private search = new Subject<any>();

  constructor(private fb: FormBuilder,
    private adminService: AdminService,
    private localStorage: LocalStorageService,
    private spinner: NgxSpinnerService,
    private titleService: Title) {
  }

  semesterForm = this.fb.group({
    semesterName: ['', [Validators.required]],
    startDate: ['', [Validators.required]],
    endDate: ['', [Validators.required]]
  });
  semesterEditForm = this.fb.group({
    semesterName: ['', [Validators.required]],
    startDate: ['', [Validators.required]],
    endDate: ['', [Validators.required]]
  })
  ngOnInit(): void {
    this.titleService.setTitle('Semesters | Workshop Evaluation');
    this.getAllSemester();
  }
  searchTerm(term: string) {
    this.search.next(term);
  }

  searchSemester() {
    this.listSemester = [];
    this.Semester$ = this.search.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => { this.spinner.show() }),
      switchMap((term: string) =>
        this.adminService.searchSemester(this.page, term, this.searchByActivated)))
    this.Semester$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.activated = item.activated ? 1 : 0;
        item.startTime = new Date(item.startTime * 1000).toDateString();
        item.endTime = new Date(item.endTime * 1000).toDateString();
      })
      this.listSemesterOnSearch = data[2];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
    });
  }
  getDataActivated(event) {
    if (event == "All Status") {
      this.searchByActivated = "";
    } else if (event == "Active") {
      this.searchByActivated = "true";
    } else if (event == "De-Active") {
      this.searchByActivated = "false";
    }
    this.spinner.show();
    this.Semester$ = this.adminService.searchSemester(this.page, "", this.searchByActivated);
    this.Semester$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.activated = item.activated ? 1 : 0;
        item.startTime = new Date(item.startTime * 1000).toDateString();
        item.endTime = new Date(item.endTime * 1000).toDateString();
      })
      this.listSemesterOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
    });
  }
  getAllSemester() {
    this.spinner.show();
    this.listSemester = [];
    this.Semester$ = this.adminService.searchSemester(this.page, "", this.searchByActivated);
    this.Semester$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.activated = item.activated ? 1 : 0;
        item.startTime = new Date(item.startTime * 1000).toDateString();
        // .toLocaleDateString('vi-vn');
        item.endTime = new Date(item.endTime * 1000).toDateString();
        // .toLocaleDateString('vi-vn');
      })
      this.listSemester = data[2];
      this.listSemesterOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
    this.searchSemester();
  }

  showModal(): void {
    this.isModalShown = true;
  }
  onHidden(): void {
    this.isModalShown = false;
    this.isValidFormSubmitted = false;
    this.resetForm(this.semesterForm);
  }
  hideModal(): void {
    this.autoShownModal.hide();
  }
  onSubmit() {
    this.isValidFormSubmitted = true;
    if (!this.semesterForm.valid) {
      return;
    }
    let startTimeConvert = new Date(this.semesterForm.value.startDate);
    let endTimeConvert = new Date(this.semesterForm.value.endDate);
    const semester = {
      name: this.semesterForm.value.semesterName,
      startTime: startTimeConvert,
      endTime: endTimeConvert
    }
    // let startTimeConvert = new Date(semester.startTime ).toLocaleDateString('vi-vn');
    // let endTimeConvert = new Date(semester.endTime).toLocaleDateString('vi-vn');
    // console.log(startTimeConvert);
    // console.log(endTimeConvert);
    // const semesterConvert = {
    //   name: this.semesterForm.value.semesterName,
    //   startTime:startTimeConvert,
    //   endTime:endTimeConvert
    // }
    // console.log(semesterConvert);
    this.adminService.addSemester(semester)
      .subscribe(
        data => console.log(data)
        , error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        },
        () => {
          swal.fire({
            title: 'Success!',
            text: 'Create semester success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(() => {
            this.hideModal();
            this.getAllSemester();
          });

        }
      );
  }

  get semesterF() {
    return this.semesterForm.controls;
  }
  get semesterEditF() {
    return this.semesterEditForm.controls;
  }
  resetForm(form: FormGroup) {
    form.reset();
  }
  showEdit(index: number): void {
    this.isEditShown = true;
    this.listSemesterEdit = this.listSemesterOnSearch[index];
    //  this.semesterEditForm.controls['endDate'].setValue(this.listSemesterOnSearch[index].endTime);
    // this.semesterEditForm.controls['semesterName'].setValue(this.listSemesterOnSearch[index].name);
    //  this.semesterEditForm.controls['startDate'].setValue(this.listSemesterOnSearch[index].startTime);
    this.semesterEditForm.patchValue({
      semesterName: this.listSemesterEdit.name,
      startDate: new Date(this.listSemesterEdit.startTime),
      endDate: new Date(this.listSemesterEdit.endTime),
    })

  }
  onHiddenEdit(): void {
    this.isEditShown = false;
    this.resetForm(this.semesterEditForm);
  }
  hideEdit(): void {
    this.autoShownEdit.hide();
  }
  onSubmitEdit() {
    this.isValidFormSubmitted = true;
    if (!this.semesterEditForm.valid) {
      return;
    }
    const startTimeConvert = new Date(this.semesterEditForm.value.startDate);
    const endTimeConvert = new Date(this.semesterEditForm.value.endDate);
    const semester = {
      name: this.semesterEditForm.value.semesterName,
      startTime: startTimeConvert,
      endTime: endTimeConvert,
    }
    this.adminService.updateSemester(semester, this.listSemesterEdit.id)
      .subscribe(
        data => console.log(data)
        , error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        },
        () => {
          swal.fire({
            title: 'Success!',
            text: 'Update semester success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.hideEdit();
            this.getAllSemester();
          });
          
        }
      );
  }

  deActive(index) {
    swal.fire({
      title: 'Are you sure to deactivate semester?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.adminService.deactiveSemester(this.listSemesterOnSearch[index].id).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Deactivate semester success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.getAllSemester();
          });
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        })
      }
    })
  }
  Active(index) {
    swal.fire({
      title: 'Are you sure to activate semester?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.adminService.activeSemester(this.listSemesterOnSearch[index].id).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Activate semester success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.getAllSemester();
          });
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        })
      }
    })
  }
  deleteSemester(index) {
    swal.fire({
      title: 'Are you sure to delete semester?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.adminService.deleteSemester(this.listSemesterOnSearch[index].id).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Delete semester success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.getAllSemester();
          });
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        })
      }
    })
  }
  filterSemester(filterValue: string) {

    // this.listSemesterOnSearch = this.listSemesterAll.filter((d) => {
    //   const ob = JSON.stringify(d);
    //   if (ob.toLowerCase().indexOf(filterValue) !== -1) {
    //     return true;
    //   }
    //   return false;
    // });


    if (filterValue.length === 0) {
      this.getAllSemester();
    } else {
      this.listSemesterOnSearch = this.listSemesterAll.filter((d) => {
        const ob = JSON.stringify(d);
        if (ob.toLowerCase().indexOf(filterValue) !== -1) {
          return true;
        }
        else {
          return false;
        }
      });
    }
  }
  pageChanged(event: any): void {
    this.getAllSemester();
  }

}
