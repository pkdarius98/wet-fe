import { Title } from '@angular/platform-browser';
import { ModalDirective, BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Executable } from '../../../model/models';
import { Observable } from 'rxjs';
import { AdminService } from '../../../services/admin.service';
import { LocalStorageService } from 'ngx-webstorage';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-executable',
  templateUrl: './executable.component.html',
  styleUrls: ['./executable.component.css']
})
export class ExecutableComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  @ViewChild('fileInput') fileInput: ElementRef;
  modalRef: BsModalRef;
  isModalShown = false;
  focusTouched1 = false;
  isValidFormSubmitted = false;
  executables$: Observable<Executable[]>;
  executables: Executable[] = [];
  uploadedFiles: File;
  fileBase64: any;
  filesName = '';
  fileSize: any;
  excutableForm = this.fb.group({
    types: ['', Validators.required],
    description: [''],
    files: [null, Validators.required]
  });

  subjects = [
    { id: '0', itemName: 'compile' },
    { id: '1', itemName: 'run' },
    { id: '2', itemName: 'compare' }
  ];
  selectedSubject = [];

  constructor(
    private admin: AdminService,
    private fb: FormBuilder,
    private localStorage: LocalStorageService,
    private spinner: NgxSpinnerService,
    private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Executable | Workshop Evaluation');
    this.getExecutables();
  }

  getExecutables() {
    this.spinner.show();
    this.admin.getExecutable().subscribe(data => {
      this.executables = data;
      this.spinner.hide();
    });
  }

  handleFileInput(event: { target: { files: any[]; }; }) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    if (file.name.substring(file.name.lastIndexOf('.')) !== '.zip') {
      this.fileC.setValue('');
      swal.fire({
        title: 'Invalid file extension',
        text: 'Only accept zip type',
        type: 'error',
      });
    } else {
      reader.onload = () => {
        this.fileBase64 = reader.result;
      };
      this.uploadedFiles = file;
      this.filesName = file.name;
    }
  }

  fileChoosen() {
    this.fileInput.nativeElement.click();
  }

  get executeF() {
    return this.excutableForm.controls;
  }

  get fileC() {
    return this.excutableForm.get('files');
  }

  get descriptC() {
    return this.excutableForm.get('description');
  }

  resetForm(form: FormGroup) {
    form.reset();
  }

  showModal(): void {
    this.isModalShown = true;
  }

  getDashBoardColor() {
    return this.localStorage.retrieve('dashColor');
  }

  onHidden(): void {
    this.isModalShown = false;
    this.resetForm(this.excutableForm);
    this.filesName = '';
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

  onSubmit() {
    this.isValidFormSubmitted = true;
    if (!this.excutableForm.valid) {
      return;
    }
    const executable: Executable = {
      executableId: this.filesName.split('.')[0],
      description: this.excutableForm.value.description,
      size: this.fileSize,
      type: this.excutableForm.value.types,
      zipFileBase64: this.fileBase64.split(',')[1]
    };
    this.admin.createExecutables(executable)
    .subscribe(
      data => {
        swal.fire({
          title: 'Success!',
          text: 'Create executable success',
          type: 'success',
          confirmButtonClass: 'btn btn-success',
        });
        this.hideModal();
        this.admin.getExecutable().subscribe(it => {
          this.executables = it;
        });
      }
    , error => {
      swal.fire({
        title: error.error.apierror.status,
        text: error.error.apierror.message,
        type: 'error',
      });
    }
    );
  }
}
