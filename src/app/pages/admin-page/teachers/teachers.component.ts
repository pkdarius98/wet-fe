import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { LocalStorageService } from 'ngx-webstorage';
import { AdminService } from 'src/app/services/admin.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild } from '@angular/core';
import swal from 'sweetalert2/src/sweetalert2.js'

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  @ViewChild('autoShowSearch', { static: false }) autoShowSearch: ModalDirective;
  @ViewChild('autoShownModalEdit', { static: false }) autoShownModalEdit: ModalDirective;
  isModalShown = false;
  isSearchShow = false;
  isModalShownEdit = false;
  Teacher$: Observable<any>;
  listClass = [];
  listClassDetail = [];
  focusTouched;
  focusTouched1;
  focusTouched2;
  focusTouched3;
  focusTouched4;
  focusTouched5;
  focusTouched6;
  focusTouched7;
  focusTouched8;
  isValidFormSubmitted = false;
  checkSubmit: number;
  checkDelete: boolean = false;
  data: string;
  TeacherTable$: Observable<any[]>;
  listTeacher: any;
  listTeacherOnSearch=[];
  listTeacherDetail: any;
  listEditTeacher: any;
  page = 1;
  totalSize: number;
  TeacherTable1$: Observable<any[]>;
  listTeacherAll: any;
  private search = new Subject<any>();
  Semester$: Observable<any[]>;
  getSemester: any = [];
  getSemesterCheck = [];
  searchBySemester: string = "";
  searchByStatus: string = "";
  getStatus = ['All Status', 'Active', 'De-Active'];
  isPageShow: boolean = false;
  selectedProvinceSemester = 'All Semester';
  selectedProvinceStatus = 'Active';
  maxSize = 5;
  constructor(private fb: FormBuilder,
    private adminService: AdminService,
    private router: Router,
    private localStorage: LocalStorageService,
    private spinner: NgxSpinnerService,
    private titleService: Title) { }
  ngOnInit(): void {
    this.titleService.setTitle('Teachers | Workshop Evaluation');
    this.getAllTeacher();
    this.getSemesterCheck.push("All Semester");
    this.getAllSemester();
  }
  teacherForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    fullName: ['', [Validators.required]],
    username: ['', [Validators.required]],
  });
  teacherEditForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    fullName: ['', [Validators.required]],
    username: ['', [Validators.required]],
    status: ['', [Validators.required]],
    classes: ['',]
  });
  searchTerm(term: string) {
    this.search.next(term);
  }
  getAllSemester() {
    this.Semester$ = this.adminService.searchSemester(this.page, "", "");
    this.Semester$.subscribe((data: any[]) => {

      this.getSemester = data[2];
      this.getSemester.forEach(item => {
        this.getSemesterCheck.push(item.name);
      })
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
    });
  }
  get_data(event) {
    if (event == "All Semester") {
      this.searchBySemester = "";
    } else {
      this.searchBySemester = event;
    }
    this.spinner.show();
    this.TeacherTable$ = this.adminService.searchTeacher(this.page, "", this.searchBySemester, this.searchByStatus);
    this.TeacherTable$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.enabled = item.enabled ? 1 : 0;
      })
      this.listTeacherOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
    });
  }
  getStatusData(event) {
    if (event == "Active") {
      this.searchByStatus = '';
    } else if (event == "All Status") {
      this.searchByStatus = '____';
    }
    else {
      this.searchByStatus = '__0_';
    }
    this.spinner.show();
    this.TeacherTable$ = this.adminService.searchTeacher(this.page, "", this.searchBySemester, this.searchByStatus);
    this.TeacherTable$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.enabled = item.enabled ? 1 : 0;
      })
      this.listTeacherOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
    });
  }

  searchTeacher() {
    this.TeacherTable$ = this.search.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => { this.spinner.show() }),
      switchMap((term: string) =>
        this.adminService.searchTeacher(this.page, term, this.searchBySemester, this.searchByStatus)
      )
    )
    this.TeacherTable$.subscribe((data: any[]) => {
      this.spinner.hide()
      data[2].forEach(item => {
        item.enabled = item.enabled ? 1 : 0;
      })
      this.listTeacherOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
    });
  }
  getAllTeacher() {
    this.spinner.show();
    this.TeacherTable$ = this.adminService.searchTeacher(this.page, "", this.searchBySemester, this.searchByStatus);
    this.TeacherTable$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.enabled = item.enabled ? 1 : 0;
      })
      this.listTeacher = data[2];
      this.listTeacherOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
    });
    this.searchTeacher();
  }

  showModal(): void {
    this.isModalShown = true;
  }
  onHidden(): void {
    this.isModalShown = false;
    this.isValidFormSubmitted = false;
    this.resetForm(this.teacherForm);
  }
  hideModal(): void {
    this.autoShownModal.hide();
  }
  onSubmit() {
    this.isValidFormSubmitted = true;
    if (!this.teacherForm.valid) {
      return;
    }
    const teacher = {
      email: this.teacherForm.value.email,
      fullName: this.teacherForm.value.fullName,
      username: this.teacherForm.value.username
    }
    this.adminService.addTeacher(teacher)
      .subscribe(
        data => console.log(data)
        , error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        },
        () => {
          swal.fire({
            title: 'Success!',
            text: 'Create teacher success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(() => {
            this.hideModal();
            this.getAllTeacher();
          });

        }
      );
  }
  resetForm(form: FormGroup) {
    form.reset();
  }
  get teacherF() {
    return this.teacherForm.controls;
  }
  get teacherEditF() {
    return this.teacherEditForm.controls;
  }
  onKey(event) {
    this.data = event.target.value;
    //this.teacherForm.get('username').setValue(this.data.substring(0, this.data.lastIndexOf('@')));
    this.teacherForm.get('email').setValue(this.data + '@fpt.edu.vn');
  }
  filterTeacher(filterValue: string) {
    // this.listTeacherOnSearch = this.listTeacher.filter((d) => {
    //   const ob = JSON.stringify(d);
    //   if (ob.toLowerCase().indexOf(filterValue) !== -1) {
    //     return true;
    //   }
    //   return false;
    // });
    if (filterValue.length === 0) {
      this.getAllTeacher();
    } else {
      this.listTeacherOnSearch = this.listTeacherAll.filter((d) => {
        const ob = JSON.stringify(d);
        if (ob.toLowerCase().indexOf(filterValue) !== -1) {
          return true;
        }
        return false;
      });
    }

  }

  showModalEdit(index): void {
    let teacher = [];
    let teacherDetail = [];
    let statusConvert: any;
    this.isModalShownEdit = true;

    this.listEditTeacher = this.listTeacherOnSearch[index];
    teacher = this.listEditTeacher.teacherClasses;
    teacher.forEach(element => {
      teacherDetail.push(element.classCode + " - " + element.subject.subjectCode + "\n");
    })
    let flag = teacherDetail.toString();
    flag = flag.split(',').join('');
    statusConvert = this.listEditTeacher.enabled;
    statusConvert = statusConvert ? "Active" : "De-Active";
    this.teacherEditForm.patchValue({
      email: this.listEditTeacher.email,
      fullName: this.listEditTeacher.fullName,
      rollNumber: this.listEditTeacher.rollNumber,
      username: this.listEditTeacher.username,
      status: statusConvert,
      classes: flag
    })
  }
  onHiddenEdit(): void {
    this.isModalShownEdit = false;
    this.isValidFormSubmitted = false;
    this.resetForm(this.teacherEditForm);
  }
  hideModalEdit(): void {
    this.autoShownModalEdit.hide();
  }
  onSubmitEdit() {
    this.isValidFormSubmitted = true;
    if (!this.teacherEditForm.valid) {
      return;
    }
    const teacher = {
      email: this.teacherEditForm.value.email,
      fullName: this.teacherEditForm.value.fullName,
      username: this.teacherEditForm.value.username
    }
    this.adminService.updateTeacherByUsername(teacher, this.listEditTeacher.username)
      .subscribe(
        data => console.log(data)
        , error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        },
        () => {
          swal.fire({
            title: 'Success!',
            text: 'Update teacher success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(() => {
            this.hideModalEdit();
            this.getAllTeacher();
          });

        }
      );

  }
  Active(index) {
    swal.fire({
      title: 'Are you sure to activate teacher?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.adminService.activeTeacher(this.listTeacherOnSearch[index].username).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Activate teacher success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.getAllTeacher();
          });
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        })
      }
    })
  }
  deActive(index) {
    swal.fire({
      title: 'Are you sure to deactivate teacher?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.adminService.deactiveTeacher(this.listTeacherOnSearch[index].username).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Deactivate teacher success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.getAllTeacher();
          });
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        })
      }
    })
  }
  pageChanged(event: any): void {
    this.getAllTeacher();
  }

}
