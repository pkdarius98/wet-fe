import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { LocalStorageService } from 'ngx-webstorage';
import { AdminService } from 'src/app/services/admin.service';
import { Router } from '@angular/router';
import { forEach } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import swal from 'sweetalert2/src/sweetalert2.js'


@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.css']
})
export class ClassesComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  @ViewChild('autoShownEdit', { static: false }) autoShownEdit: ModalDirective;
  @ViewChild('autoShownImport', { static: false }) autoShownImport: ModalDirective;
  isModalShown = false;
  isEditShown = false;
  isImportShown = false;
  isValidFormSubmitted = false;
  listClasses: any = [];
  listClassesOnSearch = [];
  listClassEdit: any;
  files = [];
  fileName = [];
  classes = [];
  Class$: Observable<any[]>;
  focusTouched4;
  focusTouched;
  focusTouched1;
  focusTouched2;
  focusTouched3;
  focusTouched6;
  focusTouched5;
  focusTouched7;
  focusTouched8;
  focusTouched9;
  focusTouched10;
  focusTouched11;
  listClassDetail: any;
  listAddStudent: any;
  listUpdateTeacher: any;
  Class1$: Observable<any[]>;
  page = 1;
  totalSize: number;
  listClassAll: any;
  detailFile: any;
  Semester$: Observable<any[]>;
  getSemester: any = [];
  getSemesterCheck = ["All Semester"];
  searchBySemester: string = "";
  searchBySubject: string = "";
  searchByActivated: string = "";
  subjectCode = ['All Subject'];
  activated = ['All Status', 'Active', 'De-Active'];
  selectedProvinceSemester = 'All Semester';
  selectedProvinceStatus = 'All Status';
  selectedProvinceSubject = 'All Subject';
  isPageShow: boolean = false;
  Subject$: Observable<any[]>;
  isGrade = [];
  maxSize = 5;
  private search = new Subject<any>();
  constructor(private fb: FormBuilder,
    private adminService: AdminService,
    private router: Router,
    private localStorage: LocalStorageService,
    private spinner: NgxSpinnerService,
    private titleService: Title) {
  }

  ngOnInit(): void {
    this.titleService.setTitle('Classes | Workshop Evaluation');
    this.getAllClasses();
    this.getAllSemester();
    this.getAllSubject();
  }
  classForm = this.fb.group({
    subjectCode: ['', [Validators.required]],
    classCode: ['', [Validators.required]],
    teacherName: ['', [Validators.required]],
    semester: ['', [Validators.required]],
    startTime: ['', [Validators.required]],
    endTime: ['', [Validators.required]],
    studentRollNumbers: ['', []]
  });
  updateTeacherForm = this.fb.group({
    username: ['', [Validators.required]]
  });
  importForm = this.fb.group({
    startDate: ['', [Validators.required]],
    endDate: ['', [Validators.required]]
  });
  searchTerm(term: string) {
    this.search.next(term);
  }


  showModal(): void {
    this.isModalShown = true;
    // this.listClassEdit = this.listClassesOnSearch[index];
    // this.classForm.patchValue({
    //   subjectCode: this.listClassEdit.subject.subjectCode,
    //   classCode: this.listClassEdit.classCode,
    //   teacherName: this.listClassEdit.teacher.username,
    //   semester: this.listClassEdit.semester.name,
    //   startTime: this.listClassEdit.startTime,
    //   endTime: this.listClassEdit.endTime,
    //   studentRollNumbers: this.classForm.value.studentRollNumbers.split(",")
    // })
  }
  onHidden(): void {
    this.isModalShown = false;
    this.isValidFormSubmitted = false;
    this.resetForm(this.classForm);
  }
  hideModal(): void {
    this.autoShownModal.hide();

  }
  onSubmit() {
    this.isValidFormSubmitted = true;
    if (!this.classForm.valid) {
      return;
    }
    // const updateClass = {
    //   subjectCode: this.classForm.value.subjectCode,
    //   classCode: this.classForm.value.classCode,
    //   teacherName: this.classForm.value.teacherName,
    //   semester: this.classForm.value.semester,
    //   startTime: this.classForm.value.startTime,
    //   endTime: this.classForm.value.endTime,
    //   studentRollNumbers: this.classForm.value.studentRollNumbers.split(",")
    // }
    // this.adminService.updateClass(updateClass,this.listClassEdit.classId)
    //   .subscribe(
    //     data => console.log(data)
    //     , error => {
    //       swal.fire({
    //         title: 'Error',
    //         text: 'Error while update class',
    //         type: 'error',
    //         confirmButtonClass: 'btn btn-info',
    //       });
    //     },
    //     () => {
    //       swal.fire({
    //         title: 'Success!',
    //         text: 'Update class success',
    //         type: 'success',
    //         confirmButtonClass: 'btn btn-success',
    //       });
    //       this.hideModal();
    //       this.getAllClasses();
    //     }
    //   );
  }
  getAllSubject() {
    this.Subject$ = this.adminService.getSubjects();
    this.Subject$.subscribe((data: any[]) => {
      data.forEach(item => {
        this.subjectCode.push(item.subjectCode);
      });
    })
  }
  getAllSemester() {
    this.Semester$ = this.adminService.searchSemester(this.page, "", "");
    this.Semester$.subscribe((data: any[]) => {
      this.getSemester = data[2];
      this.getSemester.forEach(item => {
        this.getSemesterCheck.push(item.name);
      })
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
  }
  getDataSemester(event) {
    if (event == "All Semester") {
      this.searchBySemester = "";
    } else {
      this.searchBySemester = event;
    }
    this.spinner.show();
    this.Class$ = this.adminService.searchClasses(this.page, "", this.searchBySemester, this.searchBySubject, this.searchByActivated);
    this.Class$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.activated = item.activated ? 1 : 0;
        item.startTime = new Date(item.startTime * 1000).toDateString();
        item.endTime = new Date(item.endTime * 1000).toDateString();
      })
      this.listClassesOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
  }
  getDataSubjectCode(event) {
    if (event == "All Subject") {
      this.searchBySubject = "";
    } else {
      this.searchBySubject = event;
    }
    this.spinner.show();
    this.Class$ = this.adminService.searchClasses(this.page, "", this.searchBySemester, this.searchBySubject, this.searchByActivated);
    this.Class$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.activated = item.activated ? 1 : 0;
        item.startTime = new Date(item.startTime * 1000).toDateString();
        item.endTime = new Date(item.endTime * 1000).toDateString();
      })
      this.listClassesOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
  }
  getDataActivated(event) {
    if (event == "All Status") {
      this.searchByActivated = "";
    } else if (event == "Active") {
      this.searchByActivated = "true";
    } else if (event == "De-Active") {
      this.searchByActivated = "false";
    }
    this.spinner.show();
    this.Class$ = this.adminService.searchClasses(this.page, "", this.searchBySemester, this.searchBySubject, this.searchByActivated);
    this.Class$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.activated = item.activated ? 1 : 0;
        item.startTime = new Date(item.startTime * 1000).toDateString();
        item.endTime = new Date(item.endTime * 1000).toDateString();
      })
      this.listClassesOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
  }
  searchClass() {
    this.Class$ = this.search.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => { this.spinner.show() }),
      switchMap((term: string) => this.adminService.searchClasses(this.page, term, this.searchBySemester, this.searchBySubject, this.searchByActivated)))
    this.Class$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.activated = item.activated ? 1 : 0;
        item.startTime = new Date(item.startTime * 1000).toDateString();
        item.endTime = new Date(item.endTime * 1000).toDateString();
      })
      this.listClassesOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }

    });
  }
  getAllClasses() {
    this.listClasses = [];
    this.spinner.show();
    this.Class$ = this.adminService.searchClasses(this.page, "", this.searchBySemester, this.searchBySubject, this.searchByActivated);
    this.Class$.subscribe((data: any[]) => {
      this.spinner.hide();
      data[2].forEach(item => {
        item.activated = item.activated ? 1 : 0;
        item.startTime = new Date(item.startTime * 1000).toDateString();
        // .toLocaleDateString('vi-vn');
        item.endTime = new Date(item.endTime * 1000).toDateString();
        // .toLocaleDateString('vi-vn');
      })
      this.listClasses = data[2];
      this.listClassesOnSearch = data[2];
      this.totalSize = data[1];
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
    });
    this.searchClass();
  }

  resetForm(form: FormGroup) {
    form.reset();
  }
  get classF() {
    return this.classForm.controls;
  }
  get updateTeacherF() {
    return this.updateTeacherForm.controls;
  }
  get importF() {
    return this.importForm.controls;
  }
  //Update teacher 
  updateTeacher(index: number): void {
    this.isEditShown = true;
    this.listUpdateTeacher = this.listClassesOnSearch[index];
  }
  onHiddenEdit(): void {
    this.isEditShown = false;
    this.isValidFormSubmitted = false;
    this.resetForm(this.updateTeacherForm);
  }
  hideEdit(): void {
    this.autoShownEdit.hide();
  }
  onSubmitEdit() {
    this.isValidFormSubmitted = true;
    if (!this.updateTeacherForm.valid) {
      return;
    }
    const username = this.updateTeacherForm.value.username;
    this.adminService.updateTeacher(this.listUpdateTeacher.classId, username)
      .subscribe(
        data => console.log(data)
        , error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        },
        () => {
          swal.fire({
            title: 'Success!',
            text: 'Update teacher success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(() => {
            this.hideEdit();
            this.getAllClasses();
          });

        }
      );
  }

  removeStudent(index) {
    swal.fire({
      title: 'Are you sure to remove student?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.adminService.removeStudentToClass(this.listClassDetail.classId, this.listClassDetail.students[index].rollNumber).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Remove student success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          });
          this.listClassDetail.students.splice(index, 1);
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        })
      }
    })
  }

  showDetail1(index) {
    this.listClassDetail = this.listClassesOnSearch[index];
    //console.log(this.listClassDetail.classId);
    //this.router.navigate([`/classes/classes-detail/${this.listClassDetail.classId}`]);
    this.router.navigate([`ad/classes-detail/${this.listClassDetail.classId}`], { state: { data: this.listClassDetail } });
  }

  onFileChange(event) {
    this.files = event.target.files;
    Array.prototype.forEach.call(this.files, file => {
      const fileReader = new FileReader();
      this.fileName.push(file.name);
      const [classCode, subjectCode, teacherName, semesterName] = file.name.substring(0, file.name.lastIndexOf(".")).split("_");
      const clazz = {
        classCode: classCode,
        subjectCode: subjectCode,
        teacherName: teacherName,
        semesterName: semesterName,
        students: null,
        gradeItems: null,
      }
      fileReader.readAsArrayBuffer(file);

      fileReader.onload = (e) => {
        const arrayBuffer: any = fileReader.result;
        const data = new Uint8Array(arrayBuffer);
        const arr = new Array();
        for (let i = 0; i != data.length; ++i) {
          arr[i] = String.fromCharCode(data[i]);
        }
        const bstr = arr.join("");
        const workbook = XLSX.read(bstr, { type: "binary" });

        const first_sheet_name = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[first_sheet_name];
        const arrayList = XLSX.utils.sheet_to_json(worksheet, { raw: true });
        const arrayList1 = XLSX.utils.sheet_to_json(worksheet, {
          header: 1,
          defval: '',
          blankrows: true
        });
        Object.keys(arrayList1[0]).forEach(item => {
          let value = arrayList1[0][item];
          let value_comment;
          let remove_comment;
          if(value.includes('_')){
            value_comment = value;
            remove_comment = value_comment.split('_');
            this.isGrade.push(remove_comment[0]);
          }
          
        })
        clazz.gradeItems = this.isGrade;
        this.isGrade = [];
        const students = arrayList.map((student: any) => {
          return {
            email: student.MemberCode + '@fpt.edu.vn',
            fullName: student.FullName,
            rollNumber: student.RollNumber,
            username: student.MemberCode
          }
        });
        clazz.students = students;
      }
      
      this.classes.push(clazz);
    });
    let detail = this.fileName.toString();
    console.log(this.classes);
    if (this.fileName.length > 1) {
      this.detailFile = detail.split(',')[1] + ',...';
    } else {
      this.detailFile = detail;
    }
  }

  showModalImport(): void {
    this.isImportShown = true;
  }
  onHiddenImport(): void {
    this.isImportShown = false;
    this.isValidFormSubmitted = false;
    this.resetForm(this.importForm);
    this.classes = [];
    this.fileName = [];
    this.detailFile = "";
    this.isGrade = [];
  }
  hideImport(): void {
    this.autoShownImport.hide();
  }
  onSubmitImport() {
    this.isValidFormSubmitted = true;
    if (!this.importForm.valid) {
      return;
    }
    let startTimeConvert = new Date(this.importForm.value.startDate);
    let endTimeConvert = new Date(this.importForm.value.endDate);console.log(startTimeConvert);
    this.classes.forEach(clazz => {
      // clazz.startTime = this.importForm.value.startDate.getTime();
      // clazz.endTime = this.importForm.value.endDate.getTime();
      clazz.startTime = startTimeConvert;
      clazz.endTime = endTimeConvert;
    })
    this.adminService.importClass({
      "clazzImportRequestDTOs": this.classes
    }).subscribe(
      data => console.log(data)
      , error => {
        swal.fire({
          title: 'Error',
          text: error.error.apierror.message,
          type: 'error',
          confirmButtonClass: 'btn btn-info',
        });
      },
      () => {
        swal.fire({
          title: 'Success!',
          text: 'Import class success',
          type: 'success',
          confirmButtonClass: 'btn btn-success',
        }).then(() => {
          this.hideImport();
          this.getAllClasses();
        });

      }
    );
  }
  Active(index) {
    swal.fire({
      title: 'Are you sure to activate class?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.adminService.activeClasses(this.listClassesOnSearch[index].classId).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Activate class success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(() => {
            this.getAllClasses();
          });

        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
          this.spinner.hide();
        })
      }
    })
  }
  deActive(index) {
    swal.fire({
      title: 'Are you sure to deactivate class?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.adminService.deactiveClasses(this.listClassesOnSearch[index].classId).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Deactivate class success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(() => {
            this.getAllClasses();
          });
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        })
      }
    })
  }
  pageChanged(event: any): void {
    this.getAllClasses();
  }
}
