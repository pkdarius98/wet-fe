import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalStorageService } from 'ngx-webstorage';
import { AdminService } from 'src/app/services/admin.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import swal from 'sweetalert2/src/sweetalert2.js'
@Component({
  selector: 'app-classes-detail',
  templateUrl: './classes-detail.component.html',
  styleUrls: ['./classes-detail.component.css']
})
export class ClassesDetailComponent implements OnInit {
  @ViewChild('autoShownAddStudent', { static: false }) autoShownAddStudent: ModalDirective;
  sub: any;
  id: any;
  listClassDetail = [];
  ClassDetail$: Observable<any[]>;
  isAddStudentShown = false;
  isValidFormSubmitted = false;
  focusTouched7;
  getIndexToRemove: any;
  constructor(private fb: FormBuilder,
    private _Activatedroute: ActivatedRoute,
    private adminService: AdminService,
    private localStorage: LocalStorageService,
    private spinner: NgxSpinnerService,
    private titleService: Title
  ) { }
  addStudentForm = this.fb.group({
    rollNumber: ['', [Validators.required]]
  });
  get addStudentF() {
    return this.addStudentForm.controls;
  }

  resetForm(form: FormGroup) {
    form.reset();
  }
  ngOnInit(): void {
    
    this._Activatedroute.paramMap.subscribe(item => {
      this.id = item.get('id');
    })
    this.getAllClasses();
  }
  getAllClasses() {
    this.spinner.show();
    this.ClassDetail$ = this.adminService.getClassesById(this.id);
    this.ClassDetail$.subscribe((data: any) => {
      this.spinner.hide();
      this.listClassDetail = data;
      console.log(data[2]);
      console.log(data[3].subjectCode);
      this.titleService.setTitle( data[3].subjectCode +' - ' + data[2]+' | Workshop Evaluation')
    });
    
  }
  showAddStudent(): void {
    this.isAddStudentShown = true;
  }
  onHiddenAddStudent(): void {
    this.isAddStudentShown = false;
    this.resetForm(this.addStudentForm);
  }
  hideAddStudent(): void {
    this.autoShownAddStudent.hide();
  }
  onSubmitAddStudent() {
    this.isValidFormSubmitted = true;
    if (!this.addStudentForm.valid) {
      return;
    } 
    this.adminService.addStudentToClass(this.id, this.addStudentForm.value.rollNumber)
      .subscribe(
        data => console.log(data)
        , error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        },
        () => {
          swal.fire({
            title: 'Success!',
            text: 'Add student success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(() => {
            this.hideAddStudent();
            this.getAllClasses();
          });

        }
      );
  }
  removeStudent(index) {
    this.getIndexToRemove = this.listClassDetail[6];
    //console.log(this.getIndexToRemove[index].rollNumber);
    swal.fire({
      title: 'Are you sure to remove student?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.adminService.removeStudentToClass(this.id, this.getIndexToRemove[index].rollNumber).subscribe(data => {
          swal.fire({
            title: 'Success!',
            text: 'Remove student success',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          }).then(()=>{
            this.getAllClasses();
          });
        }, error => {
          swal.fire({
            title: 'Error',
            text: error.error.apierror.message,
            type: 'error',
            confirmButtonClass: 'btn btn-info',
          });
        })
      }
    })


  }
}
