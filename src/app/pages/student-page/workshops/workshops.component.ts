import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { StudentService } from './../../../services/student.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { toLength } from 'lodash';

@Component({
  selector: 'app-workshops',
  templateUrl: './workshops.component.html',
  styleUrls: ['./workshops.component.css']
})
export class WorkshopsComponent implements OnInit {
  private sub: any;
  classId: any;
  workshop$: Observable<any[]>;
  getWorkshop=[];
  contentPDF;
  page = 1;
  totalSize: number;
  isPageShow: boolean = false;
  maxSize = 5;

  constructor(private route: ActivatedRoute,
    private student: StudentService,
    private spinner: NgxSpinnerService,
    private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Workshops | Workshop Evaluation');
    this.sub = this.route.params.subscribe(params => {
      this.classId = params.id;
      this.getAllWorkshop();
    });

  }
  getAllWorkshop() {
    this.spinner.show();
    this.workshop$ = this.student.getWorkshop(this.classId);
    this.workshop$.subscribe((data: any[]) => {
      this.spinner.hide();
      this.getWorkshop = data[2];
      console.log(data[2]);
      if (this.totalSize > 10) {
        this.isPageShow = true;
      }
    })
  }

  pageChanged(event: any): void {
    this.getAllWorkshop();
  }
  showPDF(i) {
    const win = window.open("", "_blank");
    let html = '';
    html += '<html>';
    html += '<body style="margin:0!important">';
    html += '<embed width="100%" height="100%" src="data:application/pdf;base64,' + i + '" type="application/pdf" />';
    html += '</body>';
    html += '</html>';
    setTimeout(() => {
      win.document.write(html);
    }, 100);
  }
}
