import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StudentRoutes } from './student.routing';
import { ClassesComponent } from './classes/classes.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { WorkshopsComponent } from './workshops/workshops.component';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from "ngx-bootstrap/pagination";
import { SubmissionComponent } from './submission/submission.component';
import { SubmissionDetailComponent } from './submission-detail/submission-detail.component';
import { NzListModule } from 'ng-zorro-antd/list';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TooltipModule } from 'ngx-bootstrap';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
@NgModule({
  declarations: [ClassesComponent, WorkshopsComponent, SubmissionComponent, SubmissionDetailComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(StudentRoutes),
    ModalModule.forRoot(),
    NzUploadModule,
    NzButtonModule,
    NzMessageModule,
    NzTagModule,
    NzIconModule,
    NzSelectModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    NzListModule,
    NgxSpinnerModule,
    TooltipModule.forRoot(),
    NzEmptyModule
  ]
})

export class StudentPageModule { }
