import { Title } from '@angular/platform-browser';
import { JudgingRun } from './../../../model/models';
import { forEach } from 'lodash';
import { Observable } from 'rxjs';
import { StudentService } from './../../../services/student.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-submission-detail',
  templateUrl: './submission-detail.component.html',
  styleUrls: ['./submission-detail.component.css']
})
export class SubmissionDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private student: StudentService,
    private titleService: Title) { }
  id;
  isShow: boolean = false;
  submissionDetail$: Observable<any[]>;
  submissionDetail: any;
  runTimeTotal: any;
  judgingRun: any;
  input: any;
  output: any;
  isModalShown = false;
  chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
  };
  detailRun: JudgingRun;
  outRun: any;
  
  ngOnInit(): void {
    this.titleService.setTitle('Submission Detail | Workshop Evaluation');
    this.route.params.subscribe(params => {
      this.id = params.detailId;
      this.getAllSubmissionDetail();
    });
  }
  showModal(): void {
    this.isModalShown = true;
  }

  onHidden(): void {
    this.isModalShown = false;
  }
  getAllSubmissionDetail() {
    this.submissionDetail$ = this.student.getSubmissionDetail(this.id);
    this.submissionDetail$.subscribe((data: any) => {
      this.submissionDetail = data;

      this.isShow = true;
      this.runTimeTotal = Math.round((data.judging.endTime - data.judging.startTime) * 100) / 100;
      data.submitTime = new Date(data.submitTime * 1000).toDateString();
      this.submissionDetail.judging.outputCompiled = atob(this.submissionDetail.judging.outputCompiled);
      this.judgingRun = this.submissionDetail.judging.judgingRun;
      setTimeout(() => {
        this.drawCanvas(this.submissionDetail.judging.judgingRun);
      }, 0);
    });

  }
  drawCanvas(data: JudgingRun[]) {
    const colors = [];
    const times = [];
    const labels = [];
    data.forEach((judgingRun: any, index: number) => {
      const runTime = judgingRun.runTime || 0;
      times.push(runTime);
      const result = judgingRun.runResult === 'correct';
      colors.push(result ? 'green' : 'red');
      labels.push(index);
    });

    const color = Chart.helpers.color;
    const barChartData = {
      labels,
      datasets: [{
        label: 'Run time',
        backgroundColor: colors,
        borderColor: colors,
        borderWidth: 1,
        data: times
      }]
    };
    const canvas = document.getElementById('canvas') as HTMLCanvasElement;
    const ctx = canvas.getContext('2d');
    const myChart = new Chart(ctx, {
      type: 'bar',
      data: barChartData,
      options: {
        responsive: true,
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: ''
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
    canvas.onclick = (evt) => {
      const activePoints = myChart.getElementsAtEvent(evt);
      if (activePoints[0]) {
        const chartData = activePoints[0]._chart.config.data;
        const idx = activePoints[0]._index;
        const label = chartData.labels[idx];
        const value = chartData.datasets[0].data[idx];
        this.detailRun = data[label];
        this.outRun = atob(this.detailRun.judgingRunOutput.outputRun);
        if (this.detailRun.testCase.input == null || this.detailRun.testCase.output == null) {
          this.detailRun.testCase.input = '';
          this.detailRun.testCase.output = '';
        }
        this.input = atob(this.detailRun.testCase.input);
        this.output = atob(this.detailRun.testCase.output);
        this.showModal();
      }
    };
  }

}
