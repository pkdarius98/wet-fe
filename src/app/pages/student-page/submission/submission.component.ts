import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { StudentService } from './../../../services/student.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import swal from 'sweetalert2';
@Component({
  selector: 'app-submission',
  templateUrl: './submission.component.html',
  styleUrls: ['./submission.component.css']
})
export class SubmissionComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  getFileName = [];
  fileBase64: any;
  getWorkshopId: any;
  isModalShown = false;
  selectedProvince = null;
  private sub: any;
  workshopID: any;
  classId: any
  submission$: Observable<any[]>;
  submissionList = [];
  detailSubmission: any;
  files = [];
  fileDTOs = [];
  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private student: StudentService,
    private router1: Router,
    private spinner: NgxSpinnerService,
    private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Submission | Workshop Evaluation');
    this.sub = this.route.params.subscribe(params => {
      this.workshopID = params.submitId;
      this.classId = params.id;
      this.getAllSubmission();
    });
  }
  submitForm = this.fb.group({
    files: ['', Validators.required]
  });
  getAllSubmission() {
    this.spinner.show();
    this.submission$ = this.student.getSubmission(this.workshopID);
    this.submission$.subscribe((data: any[]) => {
      this.spinner.hide();
      this.submissionList = data;
      data.forEach(item => {
        item.submitTime = new Date(item.submitTime * 1000).toDateString();
      })
    })
  }
  resetForm(form: FormGroup) {
    form.reset();
  }
  get submitF() {
    return this.submitForm.controls;
  }



  onFileChange(event) {
    this.files = event.target.files;
    Array.prototype.forEach.call(this.files, file => {
      const reader = new FileReader();
      this.getFileName.push(file.name);
      const fileDTO = {
        fileName: file.name,
        sourceCode: null,
      }
      reader.readAsDataURL(file);
      if (file.name.substring(file.name.lastIndexOf('.')) !== '.c' && file.name.substring(file.name.lastIndexOf('.')) !== '.java') {
        swal.fire({
          title: 'Invalid file extension',
          text: 'Only accept .c and .java',
          type: 'error',
        });
      } else {
        reader.onload = () => {
          this.fileBase64 = reader.result;
          this.fileBase64 = this.fileBase64.split(',')[1];
          fileDTO.sourceCode = this.fileBase64;
        };
        this.fileDTOs.push(fileDTO);

      }
    });
    const x = document.getElementById(`upload-ws`);
    x.innerHTML = this.getFileName.toString();
    if (this.getFileName.length > 1) {
      x.innerHTML = x.innerHTML.split(',')[1] + ',...';
    } else {
      x.innerHTML = this.getFileName.toString();
    }
  }
  showModal(): void {
    this.isModalShown = true;
  }
  onHidden(): void {
    this.isModalShown = false;
    this.resetForm(this.submitForm);
    this.getFileName = [];
    this.fileDTOs = [];
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

  onSubmit() {
    // this.isValidFormSubmitted = true;
    if (!this.submitForm.valid) {
      return;
    }
    const submitType = {
      classId: +this.classId,
      fileDTOs: this.fileDTOs,
      workshopId: +this.workshopID
    }
    this.student.submission(submitType).subscribe(data => {
      swal.fire({
        title: 'Success!',
        text: 'Submit success',
        type: 'success',
        confirmButtonClass: 'btn btn-success',
      }).then(()=>{
        this.hideModal();
        this.getAllSubmission();
      });
      
    }, error => {
      swal.fire({
        title: 'Error',
        text: error.error.apierror.message,
        type: 'error',
        confirmButtonClass: 'btn btn-info',
      });
    })
  }

}
