import { Routes } from '@angular/router';
import { ClassesComponent } from './classes/classes.component';
import { WorkshopsComponent } from './workshops/workshops.component';
import { SubmissionComponent } from './submission/submission.component';
import { SubmissionDetailComponent } from './submission-detail/submission-detail.component';
export const StudentRoutes: Routes = [
  {
    path: 'classes',
    component: ClassesComponent
  },
  {
    path: 'classes/:id',
    component: WorkshopsComponent
  },
  {
    path: 'classes/:id',
    component: WorkshopsComponent
  },
  {
    path: 'classes/:id/submission/:submitId',
    component: SubmissionComponent
  },
  {
    path: 'classes/:id/submission/:submitId/submission-detail/:detailId',
    component: SubmissionDetailComponent
  }
];
