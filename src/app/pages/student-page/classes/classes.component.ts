import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { StudentService } from './../../../services/student.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.css']
})
export class ClassesComponent implements OnInit {
  getClasses =[];
  Classes$: Observable<any[]>;
  Semester$: Observable<any[]>;
  constructor(private stService: StudentService,
    private spinner: NgxSpinnerService,
    private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Classes | Workshop Evaluation');
    this.getAllClasses();
  }
  getAllClasses() {
    this.spinner.show();
    this.getClasses = [];
    this.Classes$ = this.stService.getClasses();
    this.Classes$.subscribe((data: any[]) => {
      this.spinner.hide();
      data.forEach(item => {
        item.activated = item.activated ? 1 : 0;
        item.startTime = new Date(item.startTime * 1000).toDateString();
        item.endTime = new Date(item.endTime * 1000).toDateString();
      })
      this.getClasses = data;
    });
  }

}
