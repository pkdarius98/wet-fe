import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError, of, pipe} from 'rxjs';
import {map, tap, retry, catchError} from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../model/models';
import * as _ from 'lodash';
import { Chart } from 'chart.js';

@Injectable({
  providedIn: 'root'
})
export class SAServices {

  adminList: any;
  User$: Observable<User[]>;

  constructor(private http: HttpClient) {
  }

  draw(id: string, response: any) {
    var gradientChartOptionsConfigurationWithTooltipRed: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: 'nearest',
        intersect: 0,
        position: 'nearest'
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: 'rgba(29,140,248,0.0)',
              zeroLineColor: 'transparent'
            },
            ticks: {
              suggestedMin: 0,
              suggestedMax: 3,
              padding: 20,
              fontColor: '#9a9a9a',
              
            }
          }
        ],

        xAxes: [
          {
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: 'rgba(233,32,16,0.1)',
              zeroLineColor: 'transparent'
            },
            ticks: {
              padding: 20,
              fontColor: '#9a9a9a'
            }
          }
        ]
      }
    };    
    const times = response.time.sort((a,b)=>a-b).map(time=>{
      const date = new Date(time);
      let datetext = date.toTimeString().split(' ')[0].substring(0,5);
      return datetext;
    })
    const canvas =  document.getElementById(id) as HTMLCanvasElement;
    const ctx = canvas.getContext('2d');

    var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(233,32,16,0.2)');
    gradientStroke.addColorStop(0.4, 'rgba(233,32,16,0.0)');
    gradientStroke.addColorStop(0, 'rgba(233,32,16,0)'); //red colors
    var data = {
      labels: times,
      datasets: [
        {
          label: 'Data',
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: '#ec250d',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: '#ec250d',
          pointBorderColor: 'rgba(255,255,255,0)',
          pointHoverBackgroundColor: '#ec250d',
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: response.averages
        }
      ]
    };

    var myChart = new Chart(ctx, {
      type: 'line',
      data: data,
      options: gradientChartOptionsConfigurationWithTooltipRed
    });
  }

  importAdmin(listAdmin: any) {
    listAdmin.forEach( item => {
      item.enabled = item.enabled == 1;
    });
    this.http.post(`${environment.apiUrl}/api/authorization/import/`, listAdmin)
    .subscribe(data => {
      console.log(data);
    }, error => {
      throwError(error);
    });
  }

  getUsers(page:number): Observable<User[]> {
    return this.http
    .get<User[]>(`${environment.apiUrl}/api/authorization?pageNo=`+page)
    .pipe(
      retry(1),
      map(data => _.values(data)),
      catchError(err => {
        return of([]);
      })
    );
  }
  onSearchUser(page:number,keyword:string,enabled:string): Observable<User[]> {
    return this.http
    .get<User[]>(`${environment.apiUrl}/api/authorization?pageNo=`+page+"&keyword="+keyword+"&enabled="+enabled)
    .pipe(
      retry(1),
      map(data => _.values(data)),
      catchError(err => {
        return of([]);
      })
    );
  }
  getUsersAll(page:number): Observable<User[]> {
    return this.http
    .get<User[]>(`${environment.apiUrl}/api/authorization?pageSize=`+page)
    .pipe(
      retry(1),
      map(data => _.values(data)),
      catchError(err => {
        return of([]);
      })
    );
  }

  updateUser(user: User): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/authorization/${user.username}`, user);
  }
  updateActiveAdmin(username: string,action:string): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/authorization/${username}/${action}`,null);
  }
  upgradeSa(username:string):Observable<any>{
    return this.http.post(`${environment.apiUrl}/api/authorization/grantSARole/${username}`,null)
  }
  removeAdmin(username:string):Observable<any>{
    return this.http.put(`${environment.apiUrl}/api/authorization/${username}`, null);
  }
  activeUser(username:string): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/authorization/${username}/activate`, null);
  }
  deactiveUser(username:string): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/authorization/${username}/deactivate`, null);
  }
  // getSuperAdminList(): Observable<User[]> {
  //   return this.http
  //   .get<User[]>(`${environment.apiUrl}/api/authorization`)
  //   .pipe(
  //     retry(1),
  //     map(data => _.values(data)),
  //     catchError(err => {
  //       console.error(err);
  //       return of([]);
  //     })
  //   );
  // }

  addUser(user: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/authorization`, user);
  }
  getInformation(namespace: string,metricName: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/analysis`,{ params: {
      namespace: namespace,
      metricName: metricName
    }});
  }
}
