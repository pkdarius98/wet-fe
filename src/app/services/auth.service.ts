import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {LoginResponse} from '../pages/auth/login/login-response.payload';
import {LocalStorageService} from 'ngx-webstorage';
import {map, tap} from 'rxjs/operators';
import {GoogleLoginPayload} from '../pages/auth/login/google-login-payload';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn: boolean;

  getRefreshTokenPayload() {
    return {
      refreshToken: this.getRefreshToken(),
      username: this.getUserName()
    };
  }

  constructor(private http: HttpClient, private localStorage: LocalStorageService) {
  }

  setLogged(value) {
    this.isLoggedIn = value;
  }

  loginWithGoogle(googleLoginPayload: GoogleLoginPayload): Observable<boolean> {
    return this.http.post<LoginResponse>(`${environment.apiUrl}/api/auth/googleLogin`, googleLoginPayload)
      .pipe(map(data => {
        const jwt = data.authenticationToken;
        const jwtData = jwt.split('.')[1];
        const decodedJwtJsonData = window.atob(jwtData);
        const decodedJwtData = JSON.parse(decodedJwtJsonData);
        const roles = decodedJwtData.roles;
        this.localStorage.store('authenticationToken', data.authenticationToken);
        this.localStorage.store('username', data.username);
        this.localStorage.store('refreshToken', data.refreshToken);
        this.localStorage.store('expiresAt', data.expiresAt);
        this.localStorage.store('roles', roles);
        return true;
      }));
  }

  logout(callback) {
    this.http.post(`${environment.apiUrl}/api/auth/logout`, this.getRefreshTokenPayload(),
      {responseType: 'text'})
      .subscribe(() => {
        this.localStorage.clear('authenticationToken');
        this.localStorage.clear('username');
        this.localStorage.clear('refreshToken');
        this.localStorage.clear('expiresAt');
        this.localStorage.clear('roles');
        this.localStorage.clear('avatar');
        callback();
      }, error => {
        throwError(error);
      });
  }

  refreshToken() {
    return this.http.post<LoginResponse>(
      `${environment.apiUrl}/api/auth/refresh/token`, this.getRefreshTokenPayload())
      .pipe(tap(response => {
        this.localStorage.clear('authenticationToken');
        this.localStorage.clear('expiresAt');
        this.localStorage.store('authenticationToken',
          response.authenticationToken);
        this.localStorage.store('expiresAt', response.expiresAt);
      }));
  }

  getJwtToken() {
    return this.localStorage.retrieve('authenticationToken');
  }

  getRefreshToken() {
    return this.localStorage.retrieve('refreshToken');
  }

  getUserName() {
    return this.localStorage.retrieve('username');
  }

  getExpirationTime() {
    return this.localStorage.retrieve('expiresAt');
  }

  getRole() {
    return this.localStorage.retrieve('roles');
  }
}
