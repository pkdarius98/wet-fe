import {map, tap, retry, catchError } from 'rxjs/operators';
import { Observable,of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) {

   }
   getClasses(): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/student/classes`)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  getSubmission(workshopId:number): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/student/submission?workshopId=`+workshopId)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  submission(submit: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/student/submission/submit`, submit);
  }
  getSubmissionDetail(id:number): Observable<any> {
    return this.http
      .get(`${environment.apiUrl}/api/student/submission/${id}`)
      .pipe(tap(data => {
      }));
  }
  getWorkshop(classId:number): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/student/workshop?classId=`+classId)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  
}
