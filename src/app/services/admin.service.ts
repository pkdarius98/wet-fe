import { HttpClient } from '@angular/common/http';
import { Observable, throwError, of, pipe } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';
import { map, tap, retry, catchError } from 'rxjs/operators';
import { Executable } from '../model/models';
@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }
  /* Semester */
  getSemester(page: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/admin/semesters?pageNo=` + page)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  searchSemester(page: number, keyword: string, activated: string): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/admin/semesters?pageNo=` + page + "&keyword=" + keyword + "&activated=" + activated)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }

  addSemester(any: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/admin/semesters`, any);
  }
  updateSemester(any: any, id: number): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/semesters/${id}`, any);
  }
  deactiveSemester(id: number): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/semesters/${id}/deactivate`, null);
  }
  activeSemester(id: number): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/semesters/${id}/activate`, null);
  }

  deleteSemester(id): Observable<any[]> {
    return this.http
      .delete<any[]>(`${environment.apiUrl}/api/admin/semesters/${id}`)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  /* Student */
  addStudent(any: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/admin/students`, any)
  }
  getStudentByRollNumber(rollNumber: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/admin/students/get-student-by-roll-number/${rollNumber}`);
  }
  searchStudent(page: number, keyword: string, semesterName: string, enabled: string): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/admin/students?pageNo=` + page + "&keyword=" + keyword + "&semesterName=" + semesterName + "&enabled=" + enabled)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  getStudent(page: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/admin/students?pageNo=` + page)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  deactiveStudent(rollnumber: string): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/students/${rollnumber}/deactivate`, null);
  }
  activeStudent(rollnumber: string): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/students/${rollnumber}/activate`, null);
  }
  updateStudent(any: any, rollnumber: string): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/students/${rollnumber}`, any);
  }
  /* Teacher */
  addTeacher(any: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/admin/teachers`, any)
  }
  getTeacherByUsername(userName: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/admin/teachers/get-teacher-by-username/${userName}`);
  }
  upgradeToTeacher(userName: string): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/teachers/add-role-by-username/${userName}`, null);
  }

  searchTeacher(page: number, keyword: string, semesterName: string, enabled: string): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/admin/teachers?pageNo=` + page + "&keyword=" + keyword + "&semesterName=" + semesterName + "&enabled=" + enabled)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  getTeacher(page: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/admin/teachers?pageNo=` + page)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }

  deactiveTeacher(username): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/teachers/${username}/deactivate`, null);
  }
  activeTeacher(username): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/teachers/${username}/activate`, null);
  }
  updateTeacherByUsername(any: any, username): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/teachers/${username}`, any);
  }
  /* Classes */
  searchClasses(page: number, keyword: string, semesterName: string, subjectCode: string, activated: string): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/admin/classes?pageNo=` + page + "&keyword=" + keyword + "&semesterName="
        + semesterName + "&subjectCode=" + subjectCode + "&activated=" + activated)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  getClasses(page: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/admin/classes?pageNo=` + page)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  getClassesById(id): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/admin/classes/${id}`)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  addClasses(any: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/admin/classes`, any)
  }
  // addStudentToClass(id, rollNumber) {
  //   return this.http.patch(`${environment.apiUrl}/api/classes/${id}/add-student-by-roll-number`, { rollNumber: rollNumber });
  // }
  addStudentToClass(id, rollNumber) {
    return this.http.patch(`${environment.apiUrl}/api/admin/classes/${id}/add-student/${rollNumber}`, null);
  }
  removeStudentToClass(id, rollNumber) {
    return this.http.patch(`${environment.apiUrl}/api/admin/classes/${id}/delete-student/${rollNumber}`, null);
  }
  updateTeacher(id, username) {
    return this.http.patch(`${environment.apiUrl}/api/admin/classes/${id}/update-teacher/${username}`, null);
  }
  updateClass(any: any, id) {
    return this.http.patch(`${environment.apiUrl}/api/admin/classes/${id}`, null);
  }
  importClass(any: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/admin/classes/import-classes`, any)
  }
  activeClasses(id: number): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/classes/${id}/activate`, null);
  }
  deactiveClasses(id: number): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/api/admin/classes/${id}/deactivate`, null);
  }

  createSubject(subject: any) {
    return this.http.post(`${environment.apiUrl}/api/admin/subjects`, subject);
  }

  //Language
  getLanguage(page: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/admin/languages?pageNo=` + page)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }
  getNoLanguage(): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiUrl}/api/admin/languages/executables/noLanguage`);
  }
  addLanguage(any: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/admin/languages`, any)
  }
  //Executable
  getExecutable(): Observable<Executable[]> {
    return this.http
      .get<Executable[]>(`${environment.apiUrl}/api/executables`)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }

  getSubjects(): Observable<any[]> {
    return this.http.get(`${environment.apiUrl}/api/admin/subjects`)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }

  getLanguages(): Observable<any[]> {
    return this.http.get(`${environment.apiUrl}/api/admin/subjects/languages`)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }

  getExecutableById(id: string): Observable<Executable> {
    return this.http
      .get<Executable>(`${environment.apiUrl}/api/executables/${id}`)
      .pipe(
        retry(1),
        map(data => data),
        catchError(err => {
          return of({});
        })
      );
  }

  createExecutables(executable: Executable): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/admin/executables`, executable);
  }

  editExecutable(executable: Executable): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/admin/executables`, executable);
  }

}
