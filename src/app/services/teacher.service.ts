import { Injectable } from '@angular/core';
import {
  Subject,
  WorkshopOv,
  Workshop,
  TestCase,
  WorkshopZipfile,
  Category,
  TestcaseZipFile
} from '../model/models';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError, of, pipe } from 'rxjs';
import { Class } from '../model/models';
import { map, tap, retry, catchError } from 'rxjs/operators';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http: HttpClient) { }

  getClasses(): Observable<Class[]> {
    return this.http
      .get<Class[]>(`${environment.apiUrl}/api/teacher/classes`)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }

  getListWorkShop(pageIndex?: number, perPage?: number, subjectCode = '', category = ''): Observable<any> {
    const formString = `pageIndex=${pageIndex}&perPage=${perPage}&subjectCode=${subjectCode}&category=${category}`;
    const opts = { params: new HttpParams({ fromString: formString }) };
    return this.http
      .get<any>(`${environment.apiUrl}/api/teacher/workshops`, opts)
      .pipe(
        retry(1),
        map(data => data),
        catchError(err => {
          return of([]);
        })
      );
  }

  getWorkshopById(id: number): Observable<Workshop> {
    return this.http
      .get<Workshop>(`${environment.apiUrl}/api/teacher/workshops/${id}`)
      .pipe(
        retry(1),
        map(data => data),
        catchError(err => {
          return of({});
        })
      );
  }

  getTestCaseById(id: number): Observable<TestCase[]> {
    return this.http
      .get<TestCase[]>(`${environment.apiUrl}/api/teacher/workshops/${id}/testCases`)
      .pipe(
        retry(1),
        map(data => data),
        catchError(err => {
          return of([]);
        })
      );
  }

  getCategory(subjectCode: string, categoryName?: string): Observable<Category[]> {
    if (!subjectCode.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    return this.http
      .get<Category[]>(`${environment.apiUrl}/api/teacher/categories?subjectCode=${subjectCode}`)
      .pipe(
        map(items => items),
        catchError(err => {
          return of([]);
        })
      );
  }

  getAllSubject(): Observable<Subject[]> {
    return this.http
      .get<Subject[]>(`${environment.apiUrl}/api/teacher/subject`)
      .pipe(
        retry(1),
        map(data => data),
        catchError(err => {
          return of([]);
        })
      );
  }

  submission(submit: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/teacher/submission/submit`, submit);
  }

  getSubmission(page: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}/api/teacher/submission/my-submissions?pageNo=` + page)
      .pipe(
        retry(1),
        map(data => _.values(data)),
        catchError(err => {
          return of([]);
        })
      );
  }

  getDetailSubmission(classId: number, submissionId: number): Observable<any> {
    const formString = `classId=${classId}&submissionId=${submissionId}`;
    const opts = { params: new HttpParams({ fromString: formString }) };
    return this.http
      .get<any>(`${environment.apiUrl}/api/teacher/submission/workshop-submission`, opts)
      .pipe(
        retry(1),
        map(data => data),
        catchError(err => {
          return of([]);
        })
      );
  }

  getClassSubmission(classId: number, workshopId: number, pageIndex = 1, perPage = 10): Observable<any> {
    const formString = `pageNo=${pageIndex}&perPage=${perPage}&classId=${classId}&workshopId=${workshopId}`;
    const opts = { params: new HttpParams({ fromString: formString }) };
    return this.http
      .get<any>(`${environment.apiUrl}/api/teacher/submission/workshop-submissions`, opts)
      .pipe(
        retry(1),
        map(data => data),
        catchError(err => {
          return of([]);
        })
      );
  }

  getUnImportWorkshop(classId: number): Observable<any> {
    const formString = `classId=${classId}`;
    const opts = { params: new HttpParams({ fromString: formString }) };
    return this.http
      .get<any>(`${environment.apiUrl}/api/teacher/workshops/unImported`, opts)
      .pipe(
        retry(1),
        map(data => data),
        catchError(err => {
          return of([]);
        })
      );
  }

  getScoreBoard(classId: number): Observable<any> {
    const formString = `classId=${classId}`;
    const opts = { params: new HttpParams({ fromString: formString }) };
    return this.http
      .get<any>(`${environment.apiUrl}/api/teacher/classes/scoreboard`, opts)
      .pipe(
        retry(1),
        map(data => data),
        catchError(err => {
          return of([]);
        })
      );
  }

  getClassWorkshop(classId: number, pageIndex?: number, perPage?: number): Observable<any> {
    const formString = `pageIndex=${pageIndex}&perPage=${perPage}&classId=${classId}`;
    const opts = { params: new HttpParams({ fromString: formString }) };
    return this.http
      .get<any>(`${environment.apiUrl}/api/teacher/workshops/class-workshops`, opts)
      .pipe(
        retry(1),
        map(data => data),
        catchError(err => {
          return of([]);
        })
      );
  }

  getUnAssignedGradeItems(id: number): Observable<any> {
    return this.http
      .get<any>(`${environment.apiUrl}/api/teacher/classes/${id}/unAssignedGradeItems`)
      .pipe(
        retry(1),
        map(data => data),
        catchError(err => {
          return of([]);
        })
      );
  }

  getSubmissionDetail(id: number): Observable<any> {
    return this.http
      .get(`${environment.apiUrl}/api/teacher/submission/${id}`)
      .pipe(tap(data => {
      }));
  }

  actionToClassWorkshop(classId: number, workshopId: number, gradeItemId: number, addFlag: boolean) {
    const formString = `addFlag=${addFlag}&gradeItemId=${gradeItemId}`;
    const opts = { params: new HttpParams({ fromString: formString }) };
    return this.http.patch(`${environment.apiUrl}/api/teacher/classes/${classId}/update-workshop/${workshopId}`, null, opts);
  }

  deleteTestCase(workshopID, testcaseID): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/teacher/workshops/${workshopID}/testCases/${testcaseID}/delete`, null);
  }

  upRank(workshopID, testcaseID): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/teacher/workshops/${workshopID}/testCases/${testcaseID}/up`, null);
  }

  downRank(workshopID, testcaseID): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/teacher/workshops/${workshopID}/testCases/${testcaseID}/down`, null);
  }

  addTestCase(testcase: TestCase): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/teacher/testCases`, testcase);
  }

  editTestCase(testcases: TestCase[], workshopId: any): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/teacher/testCases/${workshopId}`, testcases);
  }

  uploadZipFile(file: WorkshopZipfile[]): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/teacher/workshops/import`, file);
  }

  uploadTestCase(file: TestcaseZipFile, workshopId: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/teacher/testCases/import/${workshopId}`, file);
  }

  editWorkshop(workshop: Workshop): Observable<any> {
    return this.http.put(`${environment.apiUrl}/api/teacher/workshops`, workshop);
  }

  addWorkshop(workshop: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/teacher/workshops`, workshop);
  }
}
