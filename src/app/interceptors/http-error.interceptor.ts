import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';



export class HttpErrorInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(
                retry(1),
                catchError((error: HttpErrorResponse) => {
                    let errorMessage = '';
                    let errorTitle = '';
                    console.log(error.error);
                    if (error.error instanceof ErrorEvent) {
                        // client-side error
                        errorTitle = 'Error';
                        errorMessage = `Error: ${error.error.message}`;
                    } else {
                        // server-side error
                        console.log(error.error);
                        if (error.error.hasOwnProperty('apierror')) {
                            errorMessage = error.error.apierror.message;
                        } else {
                            errorMessage = error.error;
                        }
                    }
                    Swal.fire({
                        title: errorTitle,
                        text: errorMessage,
                        type: 'error'
                    });
                    return throwError(errorMessage);
                })
            );
    }
}
