export interface Class {
    classId?: number;
    activated?: boolean;
    classCode?: string;
    semester?: Semester;
    endTime?: any;
    startTime?: any;
    subjectCode?: string;
}

export interface Semester {
    semesterId?: any;
    name?: string;
}

export interface Executable {
    executableId?: string;
    type?: string;
    md5Sum?: string;
    description?: string;
    zipFileBase64?: any;
    size?: number;
}

export interface User {
    fullName?: string;
    username?: string;
    email?: string;
    roles?: string;
    enabled?: boolean;
}

export interface WorkshopOv {
    workshopId?: number;
    name?: string;
    action?: number;
}

export interface TestCase {
    description?: string;
    image?: any;
    imageThumb?: any;
    imageType?: any;
    input?: any;
    md5SumInput?: any;
    md5SumOutput?: any;
    output?: any;
    point?: number;
    rank?: number;
    sample?: boolean;
    testcaseid?: any;
    workshopId?: any;
}

export interface Workshop {
    workshopId?: number;
    name?: string;
    timeLimit?: number;
    memoriesLimit?: number;
    outputLimit?: number;
    content?: string;
    docFile?: string;
    createdBy?: string;
    specialCompare?: string;
    specialCompareArgs?: string;
    specialRun?: string;
    testCases?: TestCase[];
    combinedRunCompare?: boolean;
    subjectCode?: string;
    categories?: string[];
}

export interface WorkshopZipfile {
    fileName?: any;
    zipFileBase64?: string;
}

export interface Category {
    categoryId?: string;
    categoryName?: string;
}

export interface Subject {
    subjectCode?: string;
    actived?: boolean;
}

export interface TestcaseZipFile {
    data: string;
    fileName: string;
}

export interface WorkshopUpload {
    errors: string[];
    fail: number;
    savedWorkshops: any;
    success: number;
}

export interface JudgingRun {
    judgingRunId: any;
    judgingRunOutput: JudgingRunOutput;
    runResult: string;
    runTime: number;
    testCase:TestCase;
}

export interface JudgingRunOutput {
    metadata: any;
    outputDifferent: any;
    outputError: any;
    outputRun: any;
    outputSystem: any;
}
