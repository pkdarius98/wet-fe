export enum Role {
    Admin = 'ROLE_AD',
    SuperAdmin = 'ROLE_SA',
    Teacher = 'ROLE_TC',
    Student = 'ROLE_ST'
}

export enum SelectionType {
    single = 'single',
    multi = 'multi',
    multiClick = 'multiClick',
    cell = 'cell',
    checkbox = 'checkbox'
}
