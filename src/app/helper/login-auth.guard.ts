import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginAuthGuard implements CanActivate {

  roleByRank = {
    ROLE_SA: 4,
    ROLE_AD: 3,
    ROLE_TC: 2,
    ROLE_SR: 1
  };

  constructor(
    private router: Router,
    private authenticationService: AuthService
) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const userName = this.authenticationService.getUserName();
      if (userName) {
        const roles = this.authenticationService.getRole();
        if (roles !== null && roles.trim() !== '') {
          const roleList = roles.split(',');
          const listRank = [];
          roleList.forEach(role => {
            listRank.push(this.roleByRank[role.trim()]);
          });
          const maxRank = Math.max(...listRank);
          if (maxRank === 4) {
            this.router.navigate(['sa', 'role']);
          } else if (maxRank === 3) {
            this.router.navigate(['ad', 'semester']);
          } else if (maxRank === 2) {
            this.router.navigate(['tc', 'workshops']);
          } else {
            this.router.navigate(['st', 'classes']);
          }
          return false;
        }
      }
      return true;
  }
}
